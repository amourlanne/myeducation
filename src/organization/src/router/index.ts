import {createRouter, createWebHistory, RouteRecordRaw} from "vue-router";
import axios from "axios";
import organizationResolver from "@/router/resolvers/organizationResolver";
import Home from "@/views/Home.vue";
import NotFound from "@/views/NotFound.vue";
import StudentLayout from "@/views/student/StudentLayout.vue";
import ProfessorLayout from "@/views/professor/ProfessorLayout.vue";
import authenticationResolver from "@/router/resolvers/authenticationResolver";
import authenticationGuard from "@/router/guards/authenticationGuard";
import StudentSessionLayout from "@/views/student/session/StudentSessionLayout.vue";
import ResetPasswordRequest from "@/views/ResetPasswordRequest.vue";
import ResetPassword from "@/views/ResetPassword.vue";
import StudentSessionTeachings from "@/views/student/session/teaching/StudentSessionTeachings.vue";
import store from "../store";
import StudentSessionList from "@/views/student/StudentSessionList.vue";
import OrganizationLayout from "@/views/admin/OrganizationLayout.vue";

import OrganizationTrainingGroupList from "@/views/admin/training/group/OrganizationTrainingGroupList.vue";
import OrganizationTrainingGroupStudentList
    from "@/views/admin/training/group/student/OrganizationTrainingGroupStudentList.vue";
import OrganizationTrainingGroupStudentAttach
    from "@/views/admin/training/group/student/OrganizationTrainingGroupStudentAttach.vue";
import OrganizationTrainingGroupCreate
    from "@/views/admin/training/group/OrganizationTrainingGroupCreate.vue";
import OrganizationTrainingPathList
    from "@/views/admin/training/path/OrganizationTrainingPathList.vue";
import OrganizationTrainingPathCreate
    from "@/views/admin/training/path/OrganizationTrainingPathCreate.vue";
import OrganizationTrainingPathGroupList
    from "@/views/admin/training/path-group/OrganizationTrainingPathGroupList.vue";
import OrganizationTrainingPathGroupCreate
    from "@/views/admin/training/path-group/OrganizationTrainingPathGroupCreate.vue";
import OrganizationTrainingPathGroupEdit
    from "@/views/admin/training/path-group/OrganizationTrainingPathGroupEdit.vue";
import OrganizationTrainingCourseList
    from "@/views/admin/training/course/OrganizationTrainingCourseList.vue";
import OrganizationTrainingCourseCreate
    from "@/views/admin/training/course/OrganizationTrainingCourseCreate.vue";
import OrganizationTrainingStudentList
    from "@/views/admin/training/student/OrganizationTrainingStudentList.vue";
import OrganizationTrainingCourseTeachingCreate
    from "@/views/admin/training/course/teaching/OrganizationTrainingCourseTeachingCreate.vue";
import OrganizationTrainingCourseTeachingList
    from "@/views/admin/training/course/teaching/OrganizationTrainingCourseTeachingList.vue";
import OrganizationTrainingTeachingList
    from "@/views/admin/training/teaching/OrganizationTrainingTeachingList.vue";
import OrganizationTrainingCourseTeachingEdit
    from "@/views/admin/training/course/teaching/OrganizationTrainingCourseTeachingEdit.vue";
import OrganizationTrainingTestList
    from "@/views/admin/training/test/OrganizationTrainingTestList.vue";
import OrganizationTrainingTeachingEdit
    from "@/views/admin/training/teaching/OrganizationTrainingTeachingEdit.vue";
import OrganizationTrainingTeachingTestList
    from "@/views/admin/training/teaching/test/OrganizationTrainingTeachingTestList.vue";
import OrganizationTrainingTeachingTestCreate
    from "@/views/admin/training/teaching/test/OrganizationTrainingTeachingTestCreate.vue";
import OrganizationTrainingTeachingTestEdit
    from "@/views/admin/training/teaching/test/OrganizationTrainingTeachingTestEdit.vue";
import OrganizationTrainingTestEdit
    from "@/views/admin/training/test/OrganizationTrainingTestEdit.vue";
import OrganizationTrainingTestMarkEdit
    from "@/views/admin/training/test/mark/OrganizationTrainingTestMarkEdit.vue";
import OrganizationTrainingSession from "@/views/admin/training/OrganizationTrainingSession.vue";
import OrganizationTrainingSessionLayout from "@/views/admin/training/OrganizationTrainingSessionLayout.vue";
import StudentMarks from "@/views/student/session/test/StudentMarks.vue";

const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        component: {
            template: `<router-view></router-view>`
        },
        meta: {
            requiresAuth: true
        },
        children: [
            {
                path: '',
                name: 'home',
                component: Home as any,
                beforeEnter: (to, from, next) => {
                    const user = store.getters.currentUser;
                    switch (user?.type) {
                        case "student":
                            return next({ name: "student", replace: true });
                        case "professor":
                            return next({ name: "professor", replace: true });
                        case "admin":
                            return next({ name: "admin", replace: true });
                    }
                    next();
                }
            },
            {
                path: 'student',
                component: StudentLayout as any,
                meta: {
                    type: 'student'
                },
                children: [
                    {
                        name: 'student',
                        path: '',
                        component: StudentSessionList,
                    },
                    // {
                    //     name: 'student-teaching',
                    //     path: 'teachings/:teachingId',
                    //     component: StudentTeaching as any,
                    //     children: [
                    //         {
                    //             name: 'student-teaching-tests',
                    //             path: 'tests',
                    //             component: StudentTeachingTests as any
                    //         }
                    //     ]
                    // },
                    {
                        path: ':schoolYear',
                        component: StudentSessionLayout as any,
                        children: [
                            {
                                name: 'student-session',
                                path: '',
                                redirect: { name: 'student-courses'},
                            },
                            {
                                name: 'student-courses',
                                path: 'courses',
                                component: StudentSessionTeachings
                            },
                            {
                                name: 'student-marks',
                                path: 'marks',
                                component: StudentMarks as any
                            },
                        ]
                    }
                ]
            },
            {
                path: 'professor',
                name: 'professor',
                component: ProfessorLayout,
                meta: {
                    type: 'professor'
                },
            },
            {
                path: 'admin',
                component: OrganizationLayout,
                meta: {
                    type: 'admin'
                },
                children: [
                    {
                        path: "",
                        name: "admin",
                        component: () => import("../views/admin/OrganizationEdit.vue"),
                    },
                    {
                        path: "trainings",
                        name: "admin-organization-trainings",
                        component: () => import("../views/admin/OrganizationTrainingList.vue"),
                    },
                    {
                        path: "students",
                        name: "admin-organization-students",
                        component: () => import("../views/admin/users/student/OrganizationStudentList.vue"),
                    },
                    {
                        path: "professors",
                        name: "admin-organization-professors",
                        component: () => import("../views/admin/users/professor/OrganizationProfessorList.vue"),
                    },
                    {
                        path: "admins",
                        component: {
                            template: `<router-view></router-view>`
                        },
                        children: [
                            {
                                path: "",
                                name: "admin-organization-admins",
                                component: () => import("../views/admin/users/admin/OrganizationAdminList.vue"),
                            },
                            {
                                path: "create",
                                name: "admin-organization-admin-create",
                                component: () => import("../views/admin/users/admin/OrganizationAdminCreate.vue"),
                            },
                            {
                                path: ":email",
                                name: "admin-organization-admin-edit",
                                component: () => import("../views/admin/users/admin/OrganizationAdminEdit.vue"),
                            },
                        ]
                    },
                    {
                        path: ":trainingSlug",
                        component: () => import("../views/admin/training/OrganizationTrainingLayout.vue"),
                        children: [
                            {
                                path: "",
                                name: "admin-organization-training",
                                component: () => import("../views/admin/training/OrganizationTrainingEdit.vue"),
                            },
                            {
                                path: ":schoolYear",
                                component: OrganizationTrainingSessionLayout as any,
                                children: [
                                    {
                                        path: "",
                                        name: "admin-organization-training-session",
                                        component: OrganizationTrainingSession,
                                    },
                                    {
                                        path: "groups",
                                        component: {
                                            template: `<router-view></router-view>`
                                        },
                                        children: [
                                            {
                                                path: "",
                                                name: "organization-training-group-list",
                                                component: OrganizationTrainingGroupList,
                                            },
                                            {
                                                path: "create",
                                                name: "organization-training-group-create",
                                                component: OrganizationTrainingGroupCreate,
                                            },
                                            {
                                                path: ":groupSlug",
                                                name: "organization-training-group",
                                                redirect: { name: "organization-training-group-student-list" },
                                                component: () => import("../views/admin/training/group/OrganizationTrainingGroupEdit.vue"),
                                                children: [
                                                    {
                                                        path: "",
                                                        name: "organization-training-group-student-list",
                                                        component: OrganizationTrainingGroupStudentList,
                                                    },
                                                    {
                                                        path: "attach",
                                                        name: "organization-training-group-student-attach",
                                                        component: OrganizationTrainingGroupStudentAttach,
                                                    },
                                                ]
                                            },
                                        ]
                                    },
                                    {
                                        path: "paths",
                                        component: {
                                            template: `<router-view></router-view>`
                                        },
                                        children: [
                                            {
                                                path: "",
                                                name: "organization-training-path-list",
                                                component: OrganizationTrainingPathList,
                                            },
                                            {
                                                path: "create",
                                                name: "organization-training-path-create",
                                                component: OrganizationTrainingPathCreate,
                                            },
                                            {
                                                path: ":pathId",
                                                name: "organization-training-path",
                                                component: () => import("../views/admin/training/path/OrganizationTrainingPathEdit.vue"),
                                            },
                                        ]
                                    },
                                    {
                                        path: "path-groups",
                                        component: {
                                            template: `<router-view></router-view>`
                                        },
                                        children: [
                                            {
                                                path: "",
                                                name: "organization-training-path-group-list",
                                                component: OrganizationTrainingPathGroupList,
                                            },
                                            {
                                                path: "create",
                                                name: "organization-training-path-group-create",
                                                component: OrganizationTrainingPathGroupCreate,
                                            },
                                            {
                                                path: ":pathGroupId",
                                                name: "organization-training-path-group",
                                                component: OrganizationTrainingPathGroupEdit as any,
                                            },
                                        ]
                                    },
                                    {
                                        path: "students",
                                        name: "organization-training-student-list",
                                        component: OrganizationTrainingStudentList
                                    },
                                    {
                                        path: "courses",
                                        component: {
                                            template: `<router-view></router-view>`
                                        },
                                        children: [
                                            {
                                                path: "",
                                                name: "organization-training-course-list",
                                                component: OrganizationTrainingCourseList,
                                            },
                                            {
                                                path: "create",
                                                name: "organization-training-course-create",
                                                component: OrganizationTrainingCourseCreate,
                                            },
                                            {
                                                path: ":courseId",
                                                component: () => import("../views/admin/training/course/OrganizationTrainingCourseEdit.vue"),
                                                children: [
                                                    {
                                                        path: "",
                                                        name: "organization-training-course",
                                                        component: OrganizationTrainingCourseTeachingList,
                                                    },
                                                    {
                                                        path: "teachings/create",
                                                        name: "organization-training-course-teaching-create",
                                                        component: OrganizationTrainingCourseTeachingCreate,
                                                    },
                                                    {
                                                        path: "teachings/:groupSlug",
                                                        name: "organization-training-course-teaching-edit",
                                                        component: OrganizationTrainingCourseTeachingEdit as any,
                                                    },
                                                ]
                                            },
                                        ]
                                    },
                                    {
                                        path: "teachings",
                                        component: {
                                            template: `<router-view></router-view>`
                                        },
                                        children: [
                                            {
                                                path: "",
                                                name: "organization-training-teaching-list",
                                                component: OrganizationTrainingTeachingList
                                            },
                                            {
                                                path: ":teachingId",
                                                component: OrganizationTrainingTeachingEdit as any,
                                                children: [
                                                    {
                                                        path: "",
                                                        name: "organization-training-teaching-edit",
                                                        component: OrganizationTrainingTeachingTestList
                                                    },
                                                    {
                                                        path: "tests/create",
                                                        name: "organization-training-teaching-test-create",
                                                        component: OrganizationTrainingTeachingTestCreate
                                                    },
                                                    {
                                                        path: "tests/:testId",
                                                        name: "organization-training-teaching-test-edit",
                                                        component: OrganizationTrainingTeachingTestEdit as any,
                                                    },
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        path: "tests",
                                        component: {
                                            template: `<router-view></router-view>`
                                        },
                                        children: [
                                            {
                                                path: "",
                                                name: "organization-training-test-list",
                                                component: OrganizationTrainingTestList
                                            },
                                            {
                                                path: ":testId",
                                                name: "organization-training-test-edit",
                                                component: OrganizationTrainingTestEdit as any,
                                                children: [
                                                    {
                                                        path: "marks/:markId",
                                                        name: "organization-training-test-mark-edit",
                                                        component: OrganizationTrainingTestMarkEdit as any
                                                    },
                                                ]
                                            }
                                        ]
                                    },
                                ]
                            },

                        ]
                    }
                ]
            }
        ]
    },
    {
        path: "/login",
        name: "login",
        component: () => import("../views/Login.vue"),
    },
    {
        path: "/reset-password",
        name: "reset-password-request",
        component: ResetPasswordRequest,
    },
    {
        path: "/reset-password/reset",
        name: "reset-password",
        component: ResetPassword,
    },
    {
        path: "/reset-password/reset/:resetToken",
        beforeEnter: (to, from, next) => {
            store.commit('setResetPasswordToken', to.params.resetToken);
            next({ name: 'reset-password', replace: true });
        },
        component: {
            template: '<template></template>'
        }
    },
    {
        path: '/:pathMatch(.*)*',
        name: 'not-found',
        component: NotFound
    }
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
});

router.beforeEach(organizationResolver);

router.beforeEach(authenticationResolver);
router.beforeEach(authenticationGuard);

axios.interceptors.request
    .use(config => {
            const host = window.location.host;
            const parts = host.split('.');
            const organizationSlug = parts[0];

            if (organizationSlug) {
                config.headers.common["X-Organization-Slug"] = organizationSlug;
            }

            return config;
        },
        error => Promise.reject(error));

export default router;
