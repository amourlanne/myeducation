import {useStore} from "vuex";
import {Organization} from "@/types";

export function useSetCurrentOrganization(){
    const store = useStore();
    return (organization: Organization) => store.commit('setCurrentOrganization', organization);
}
