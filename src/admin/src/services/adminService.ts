import axios from 'axios';
import {
    Path,
    Group,
    ModelCollection,
    QueryParams,
    RouteQueryParams,
    Training,
    User,
    Course,
    Teaching,
    Test, Mark, Session, PathGroup
} from "@/types";

export default {
    login(username: string, password: string): Promise<User> {
        return new Promise((resolve, reject) => {
            axios
                .post('/admin/login', { username, password })
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationAdmin(organizationSlug: string, adminEmail: string): Promise<User> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/admins/${adminEmail}`)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    createOrganizationAdmin(organizationSlug: string, adminData: User): Promise<User> {
        return new Promise((resolve, reject) => {
            axios
                .post(`/admin/organizations/${organizationSlug}/admins`, adminData)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    updateOrganizationAdmin(organizationSlug: string, adminEmail: string, adminData: User): Promise<User> {
        return new Promise((resolve, reject) => {
            axios
                .put(`/admin/organizations/${organizationSlug}/admins/${adminEmail}`, adminData)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    deleteOrganizationAdmin(organizationSlug: string, adminEmail: string): Promise<User> {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/admin/organizations/${organizationSlug}/admins/${adminEmail}`)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationAdmins(organizationSlug: string): Promise<User[]> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/admins`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationProfessors(organizationSlug: string): Promise<User[]> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/professors`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationStudents(organizationSlug: string, params: QueryParams ): Promise<ModelCollection<User>> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/students`, {
                    params: params
                })
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainings(organizationSlug: string, params: RouteQueryParams ): Promise<ModelCollection<Training>> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings`, {
                    params: params
                })
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTraining(organizationSlug: string, trainingSlug: string ): Promise<Training> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingGroups(organizationSlug: string, trainingSlug: string, schoolYear: string, params: QueryParams ): Promise<ModelCollection<Group>> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/groups`, {
                    params: params
                })
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingGroup(organizationSlug: string, trainingSlug: string, schoolYear: string, groupSlug: string ): Promise<Group> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/groups/${groupSlug}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    createOrganizationTrainingGroup(organizationSlug: string, trainingSlug: string, schoolYear: string, groupData: Group): Promise<Group> {
        return new Promise((resolve, reject) => {
            axios
                .post(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/groups`, groupData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    updateOrganizationTrainingGroup(organizationSlug: string, trainingSlug: string, schoolYear: string, groupSlug: string, groupData: Group): Promise<Group> {
        return new Promise((resolve, reject) => {
            axios
                .put(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/groups/${groupSlug}`, groupData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    deleteOrganizationTrainingGroup(organizationSlug: string, trainingSlug: string, schoolYear: string, groupSlug: string): Promise<Group> {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/groups/${groupSlug}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingGroupStudents(organizationSlug: string, trainingSlug: string, schoolYear: string, groupSlug: string ): Promise<User[]> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/groups/${groupSlug}/students`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    detachOrganizationTrainingGroupStudent(organizationSlug: string, trainingSlug: string, schoolYear: string, groupSlug: string, studentEmail: string ) {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/groups/${groupSlug}/students/${studentEmail}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    attachOrganizationTrainingGroupStudent(organizationSlug: string, trainingSlug: string, schoolYear: string, groupSlug: string, studentEmail: string ) {
        return new Promise((resolve, reject) => {
            axios
                .post(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/groups/${groupSlug}/students/${studentEmail}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    /** Organization Training PathGroup */
    getOrganizationTrainingPathGroupList(organizationSlug: string, trainingSlug: string, schoolYear: string, params: QueryParams ): Promise<ModelCollection<PathGroup>> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/path-groups`, {
                    params: params
                })
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingPathGroup(organizationSlug: string, trainingSlug: string, schoolYear: string, pathGroupId: number ): Promise<PathGroup> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/path-groups/${pathGroupId}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    createOrganizationTrainingPathGroup(organizationSlug: string, trainingSlug: string, schoolYear: string, pathGroupData: PathGroup): Promise<PathGroup> {
        return new Promise((resolve, reject) => {
            axios
                .post(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/path-groups`, pathGroupData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    deleteOrganizationTrainingPathGroup(organizationSlug: string, trainingSlug: string, schoolYear: string, pathGroupId: number): Promise<PathGroup> {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/path-groups/${pathGroupId}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    updateOrganizationTrainingPathGroup(organizationSlug: string, trainingSlug: string, schoolYear: string, pathGroupId: number, pathGroupData: PathGroup): Promise<PathGroup> {
        return new Promise((resolve, reject) => {
            axios
                .put(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/path-groups/${pathGroupId}`, pathGroupData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    /** Organization Training Path */
    getOrganizationTrainingPathList(organizationSlug: string, trainingSlug: string, schoolYear: string, params: QueryParams ): Promise<ModelCollection<Path>> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/paths`, {
                    params: params
                })
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingPath(organizationSlug: string, trainingSlug: string, schoolYear: string, pathId: number ): Promise<Path> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/paths/${pathId}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    createOrganizationTrainingPath(organizationSlug: string, trainingSlug: string, schoolYear: string, pathData: Path): Promise<Path> {
        return new Promise((resolve, reject) => {
            axios
                .post(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/paths`, pathData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    deleteOrganizationTrainingPath(organizationSlug: string, trainingSlug: string, schoolYear: string, pathId: number): Promise<Path> {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/paths/${pathId}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    updateOrganizationTrainingPath(organizationSlug: string, trainingSlug: string, schoolYear: string, pathId: number, pathData: Path): Promise<Path> {
        return new Promise((resolve, reject) => {
            axios
                .put(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/paths/${pathId}`, pathData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingCourseList(organizationSlug: string, trainingSlug: string, schoolYear: string, params: QueryParams ): Promise<ModelCollection<Course>> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/courses`, {
                    params: params
                })
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingCourse(organizationSlug: string, trainingSlug: string, schoolYear: string, courseId: number ): Promise<Course> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/courses/${courseId}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    createOrganizationTrainingCourse(organizationSlug: string, trainingSlug: string, schoolYear: string, courseData: Course): Promise<Course> {
        return new Promise((resolve, reject) => {
            axios
                .post(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/courses`, courseData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    deleteOrganizationTrainingCourse(organizationSlug: string, trainingSlug: string, schoolYear: string, courseId: number): Promise<Course> {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/courses/${courseId}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    updateOrganizationTrainingCourse(organizationSlug: string, trainingSlug: string, schoolYear: string, courseId: number, courseData: Course): Promise<Course> {
        return new Promise((resolve, reject) => {
            axios
                .put(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/courses/${courseId}`, courseData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingStudentList(organizationSlug: string, trainingSlug: string, schoolYear: string, params: QueryParams ): Promise<ModelCollection<User>> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/students`, {
                    params: params
                })
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingTeachingList(organizationSlug: string, trainingSlug: string, schoolYear: string, params: QueryParams ): Promise<ModelCollection<Teaching>> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/teachings`, {
                    params: params
                })
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingCourseTeachingList(organizationSlug: string, trainingSlug: string, schoolYear: string, courseId: number): Promise<Teaching[]> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/courses/${courseId}/teachings`, {})
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    removeOrganizationTrainingCourseTeaching(organizationSlug: string, trainingSlug: string, schoolYear: string, courseId: number, groupSlug: string) {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/courses/${courseId}/teachings/${groupSlug}`, {})
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingCourseTeaching(organizationSlug: string, trainingSlug: string, schoolYear: string, courseId: number, groupSlug: string,): Promise<Teaching> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/courses/${courseId}/teachings/${groupSlug}`, {})
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    updateOrganizationTrainingCourseTeaching(organizationSlug: string, trainingSlug: string, schoolYear: string, courseId: number, groupSlug: string, teachingData: Teaching): Promise<Teaching> {
        return new Promise((resolve, reject) => {
            axios
                .put(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/courses/${courseId}/teachings/${groupSlug}`, teachingData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingCourseAvailableGroupList(organizationSlug: string, trainingSlug: string, schoolYear: string, courseId: number): Promise<Group[]> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/courses/${courseId}/available-groups`, {})
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    createOrganizationTrainingCourseTeaching(organizationSlug: string, trainingSlug: string, schoolYear: string, courseId: number, groupSlug: string, teachingData: Teaching): Promise<Teaching> {
        return new Promise((resolve, reject) => {
            axios
                .post(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/courses/${courseId}/teachings/${groupSlug}`, teachingData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingTestList(organizationSlug: string, trainingSlug: string, schoolYear: string, params: QueryParams ): Promise<ModelCollection<Test>> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/tests`,{
                    params: params
                })
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingTeaching(organizationSlug: string, trainingSlug: string, schoolYear: string, teachingId: number ): Promise<Teaching> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/teachings/${teachingId}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    updateOrganizationTrainingTeaching(organizationSlug: string, trainingSlug: string, schoolYear: string, teachingId: number, teachingData: Teaching): Promise<Teaching> {
        return new Promise((resolve, reject) => {
            axios
                .put(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/teachings/${teachingId}`, teachingData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingTeachingTestList(organizationSlug: string, trainingSlug: string, schoolYear: string, teachingId: number ): Promise<Test[]> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/teachings/${teachingId}/tests`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    createOrganizationTrainingTeachingTest(organizationSlug: string, trainingSlug: string, schoolYear: string, teachingId: number, testData: Test): Promise<Test> {
        return new Promise((resolve, reject) => {
            axios
                .post(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/teachings/${teachingId}/tests`, testData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    deleteOrganizationTrainingTeachingTest(organizationSlug: string, trainingSlug: string, schoolYear: string, teachingId: number, testId: number ): Promise<Test> {
        return new Promise((resolve, reject) => {
            axios
                .delete(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/teachings/${teachingId}/tests/${testId}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingTeachingTest(organizationSlug: string, trainingSlug: string, schoolYear: string, teachingId: number, testId: number ): Promise<Test> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/teachings/${teachingId}/tests/${testId}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    updateOrganizationTrainingTeachingTest(organizationSlug: string, trainingSlug: string, schoolYear: string, teachingId: number, testId: number, testData: Test): Promise<Test> {
        return new Promise((resolve, reject) => {
            axios
                .put(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/teachings/${teachingId}/tests/${testId}`, testData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingTest(organizationSlug: string, trainingSlug: string, schoolYear: string, testId: number ): Promise<Test> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/tests/${testId}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingTestMark(organizationSlug: string, trainingSlug: string, schoolYear: string, testId: number, markId: number ): Promise<Mark> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/tests/${testId}/marks/${markId}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    updateOrganizationTrainingTestMark(organizationSlug: string, trainingSlug: string, schoolYear: string, testId: number, markId: number, markData: Mark): Promise<Mark> {
        return new Promise((resolve, reject) => {
            axios
                .put(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}/tests/${testId}/marks/${markId}`, markData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    getOrganizationTrainingSession(organizationSlug: string, trainingSlug: string, schoolYear: string): Promise<Session> {
        return new Promise((resolve, reject) => {
            axios
                .get(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    createOrganizationTrainingSession(organizationSlug: string, trainingSlug: string, schoolYear: string): Promise<Session> {
        return new Promise((resolve, reject) => {
            axios
                .post(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}`)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },
    updateOrganizationTrainingSession(organizationSlug: string, trainingSlug: string, schoolYear: string, sessionData: Session): Promise<Session> {
        return new Promise((resolve, reject) => {
            axios
                .put(`/admin/organizations/${organizationSlug}/trainings/${trainingSlug}/${schoolYear}`, sessionData)
                .then(response => {
                    resolve(response?.data);
                })
                .catch(error => {
                    reject(error.response?.data);
                });
        });
    },

};
