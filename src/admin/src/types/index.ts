import { ComponentPublicInstance as BaseComponentPublicInstance } from 'vue';
import {RouteLocationNormalized as BaseRouteLocationNormalized, RouteLocationNormalizedLoaded} from "vue-router";

interface Data {
    [key: string]: unknown
}

export type Props = Data

export interface Params {
    [key: string]: string
}

export interface Environment {
    [key: string]: string
}

export type RouteQueryParams = Params

export interface QueryParams {
    [key: string]: string|number
}

export interface Route extends RouteLocationNormalizedLoaded {
    query: RouteQueryParams,
    params: Params
}

export interface RouteLocationNormalized extends BaseRouteLocationNormalized {
    query: RouteQueryParams,
    params: Params
}

export interface SetupContext {
    attrs: Data
    emit: (event: string, ...args: unknown[]) => void
}

export interface ComponentPublicInstance extends BaseComponentPublicInstance, Data {
}

export interface ModelCollection<T> {
    items: T[],
    totalItems: number,
    pages: number
}

// model
export interface Organization {
    slug: string;
    name: string;
    sector: string;
}

export interface Training {
    slug: string;
    name: string;
    sessions: Session[]
}

export interface Session {
    id: string;
    schoolYear: string;
    unitLevel: number
}

export interface Group {
    slug: string;
    name: string;
    studentCount: number;
    path?: Path
}

export interface PathGroup {
    id: number;
    name: string;
    uniqueChoice: boolean;
    mandatoryChoice: boolean;
    maxChoice: number;
    minChoice: number;
    paths: Path[];
}

export interface Path {
    id: number;
    name: string;
    pathGroup?: PathGroup
}

export interface Course {
    id: number;
    name: string;
    path?: Path
}

export interface User {
    fullName: string;
    firstName: string;
    lastName: string;
    email: string;
}

export interface Teaching {
    id?: number;
    group: Group;
    course: Course;
    professor?: User;
}

export interface Mark {
    id: number;
    student: User;
    value?: number
}

export interface Test {
    id: number;
    teaching: Teaching;
    name: string;
    creator?: User;
    marks: Mark[]
}


