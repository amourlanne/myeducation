import { createApp } from "vue";

import 'bootstrap';
import '@fortawesome/fontawesome-free/js/all';

import axios from "axios";

import "./registerServiceWorker";
import router from "./router";
import store from "./store";

import App from "./App.vue";
import {Environment} from "@/types";

declare global {
    interface Window {
        env: Environment;
    }
}

axios.defaults.withCredentials = true;
axios.defaults.baseURL = `https://api.${window.env.DOMAIN}`;

axios.interceptors.response.use(
    res => res,
    (error) => {
        const currentRoute = router.currentRoute.value;
        if (error?.response.status == 401 && currentRoute?.name !== 'login') {
            if (currentRoute) {
                store.commit('setRedirectRoute', currentRoute)
                store.commit('setCurrentUser', null)
            }
            return router.push({name: 'login'});
        }
        return Promise.reject(error);
    });

createApp(App)
    .use(store)
    .use(router)
    // .use(VuelidatePlugin as any)
    // .component('v-select', vSelect)
    .mount("#app");
