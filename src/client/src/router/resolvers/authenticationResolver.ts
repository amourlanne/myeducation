import Cookies from "js-cookie";
import {NavigationGuardNext, RouteLocationNormalized} from "vue-router";
import store from '@/store';
import userService from "@/services/userService";

const authenticationResolver = async (to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) => {

    const hasJwtHeaderPayloadCookie = !!Cookies.get(window.env.JWT_COOKIE_HP);
    const hasCurrentUser = store.getters.hasCurrentUser;

    if (hasJwtHeaderPayloadCookie && !hasCurrentUser) {
        try {
            const user = await userService.authenticate();
            store.commit('setCurrentUser', user)
        } catch (error) {
            // Cookies.remove(window.env.JWT_COOKIE_HP, { domain: `.${window.env.DOMAIN}` });
        }
    }
    next()
}

export default authenticationResolver
