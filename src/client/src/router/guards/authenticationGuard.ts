import {NavigationGuardNext, RouteLocationNormalized} from "vue-router";
import store from '@/store';

const authenticationGuard = (to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) => {

    const requiresAuth = to.meta.requiresAuth || to.matched.find(data => data.meta.requiresAuth);

    if(requiresAuth && !store.getters.hasCurrentUser) {
        return next({
            name: 'login',
            params: to.params,
        });
    }
    next()
}

export default authenticationGuard
