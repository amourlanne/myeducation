import { createApp } from "vue";

import 'bootstrap';
import '@fortawesome/fontawesome-free/js/all';

import axios from "axios";

import "./registerServiceWorker";
import router from "./router";
import store from "./store";

import App from "./App.vue";
import {Environment} from "@/types";

declare global {
    interface Window {
        env: Environment;
    }
}

axios.defaults.withCredentials = true;
axios.defaults.baseURL = `https://api.${window.env.DOMAIN}`;

createApp(App)
  .use(store)
  .use(router)
  .mount("#app");
