module.exports = {
  devServer: {
    allowedHosts: ["myeducation.local", "www.myeducation.local"],
    port: 8080
  },
  runtimeCompiler: true,
  chainWebpack: (config) => {
    // build not copy public/env.js
    config.plugin('copy').tap(() => [[
          {
            from: 'public',
            to: '',
            toType: 'dir',
            ignore: ['env.js']
          }
        ]]
    );
  }
};
