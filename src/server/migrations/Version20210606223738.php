<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210606223738 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE course_units (id INT NOT NULL, path_unit_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_1B73A13F0B4624 (path_unit_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE courses (id INT AUTO_INCREMENT NOT NULL, session_id INT NOT NULL, session_part_id INT DEFAULT NULL, unit_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_A9A55A4C613FECDF (session_id), INDEX IDX_A9A55A4C5E3BC9B6 (session_part_id), INDEX IDX_A9A55A4CF8BD700D (unit_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `groups` (id INT AUTO_INCREMENT NOT NULL, session_id INT NOT NULL, path_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(65) NOT NULL, INDEX IDX_F06D3970613FECDF (session_id), INDEX IDX_F06D3970D96C566B (path_id), UNIQUE INDEX group_slug (slug, session_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE groups_students (group_id INT NOT NULL, student_id INT NOT NULL, INDEX IDX_1F7B776DFE54D947 (group_id), INDEX IDX_1F7B776DCB944F1A (student_id), PRIMARY KEY(group_id, student_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE marks (id INT AUTO_INCREMENT NOT NULL, test_id INT NOT NULL, student_id INT NOT NULL, value DOUBLE PRECISION DEFAULT NULL, INDEX IDX_3C6AFA531E5D0459 (test_id), INDEX IDX_3C6AFA53CB944F1A (student_id), UNIQUE INDEX mark_test_student (test_id, student_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE organizations (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(65) NOT NULL, name VARCHAR(255) NOT NULL, sector ENUM(\'private\', \'public\') NOT NULL COMMENT \'(DC2Type:organization_sector)\', UNIQUE INDEX organization_slug (slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE path_groups (id INT AUTO_INCREMENT NOT NULL, session_id INT NOT NULL, name VARCHAR(255) NOT NULL, unique_choice TINYINT(1) NOT NULL, mandatory_choice TINYINT(1) NOT NULL, max_choice INT NOT NULL, min_choice INT NOT NULL, INDEX IDX_91F3A40B613FECDF (session_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE path_units (id INT NOT NULL, path_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_1CD2DC97D96C566B (path_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE paths (id INT NOT NULL, session_id INT NOT NULL, path_group_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_8BBA8611613FECDF (session_id), INDEX IDX_8BBA8611BC21F71D (path_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE session_parts (id INT AUTO_INCREMENT NOT NULL, session_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_B23D1F28613FECDF (session_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sessions (id INT AUTO_INCREMENT NOT NULL, training_id INT NOT NULL, school_year VARCHAR(255) NOT NULL, unit_level INT NOT NULL, INDEX IDX_9A609D13BEFD98D1 (training_id), UNIQUE INDEX session_training_year (training_id, school_year), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE teachings (id INT AUTO_INCREMENT NOT NULL, group_id INT NOT NULL, course_id INT NOT NULL, professor_id INT DEFAULT NULL, INDEX IDX_3E79ADC4FE54D947 (group_id), INDEX IDX_3E79ADC4591CC992 (course_id), INDEX IDX_3E79ADC47D2D84D5 (professor_id), UNIQUE INDEX teaching_group_course (group_id, course_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tests (id INT AUTO_INCREMENT NOT NULL, teaching_id INT NOT NULL, creator_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_1260FC5E899F2BB2 (teaching_id), INDEX IDX_1260FC5E61220EA6 (creator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trainings (id INT AUTO_INCREMENT NOT NULL, organization_id INT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(65) NOT NULL, INDEX IDX_66DC433032C8A3DE (organization_id), UNIQUE INDEX training_slug (organization_id, slug), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE units (id INT AUTO_INCREMENT NOT NULL, discr VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, organization_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', password VARCHAR(255) NOT NULL, salt VARCHAR(255) DEFAULT NULL, enabled TINYINT(1) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, discr VARCHAR(255) NOT NULL, INDEX IDX_1483A5E932C8A3DE (organization_id), UNIQUE INDEX user_email (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE course_units ADD CONSTRAINT FK_1B73A13F0B4624 FOREIGN KEY (path_unit_id) REFERENCES path_units (id)');
        $this->addSql('ALTER TABLE course_units ADD CONSTRAINT FK_1B73A1BF396750 FOREIGN KEY (id) REFERENCES units (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE courses ADD CONSTRAINT FK_A9A55A4C613FECDF FOREIGN KEY (session_id) REFERENCES sessions (id)');
        $this->addSql('ALTER TABLE courses ADD CONSTRAINT FK_A9A55A4C5E3BC9B6 FOREIGN KEY (session_part_id) REFERENCES session_parts (id)');
        $this->addSql('ALTER TABLE courses ADD CONSTRAINT FK_A9A55A4CF8BD700D FOREIGN KEY (unit_id) REFERENCES units (id)');
        $this->addSql('ALTER TABLE `groups` ADD CONSTRAINT FK_F06D3970613FECDF FOREIGN KEY (session_id) REFERENCES sessions (id)');
        $this->addSql('ALTER TABLE `groups` ADD CONSTRAINT FK_F06D3970D96C566B FOREIGN KEY (path_id) REFERENCES paths (id)');
        $this->addSql('ALTER TABLE groups_students ADD CONSTRAINT FK_1F7B776DFE54D947 FOREIGN KEY (group_id) REFERENCES `groups` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE groups_students ADD CONSTRAINT FK_1F7B776DCB944F1A FOREIGN KEY (student_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE marks ADD CONSTRAINT FK_3C6AFA531E5D0459 FOREIGN KEY (test_id) REFERENCES tests (id)');
        $this->addSql('ALTER TABLE marks ADD CONSTRAINT FK_3C6AFA53CB944F1A FOREIGN KEY (student_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE path_groups ADD CONSTRAINT FK_91F3A40B613FECDF FOREIGN KEY (session_id) REFERENCES sessions (id)');
        $this->addSql('ALTER TABLE path_units ADD CONSTRAINT FK_1CD2DC97D96C566B FOREIGN KEY (path_id) REFERENCES paths (id)');
        $this->addSql('ALTER TABLE path_units ADD CONSTRAINT FK_1CD2DC97BF396750 FOREIGN KEY (id) REFERENCES units (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE paths ADD CONSTRAINT FK_8BBA8611613FECDF FOREIGN KEY (session_id) REFERENCES sessions (id)');
        $this->addSql('ALTER TABLE paths ADD CONSTRAINT FK_8BBA8611BC21F71D FOREIGN KEY (path_group_id) REFERENCES path_groups (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE paths ADD CONSTRAINT FK_8BBA8611BF396750 FOREIGN KEY (id) REFERENCES units (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE session_parts ADD CONSTRAINT FK_B23D1F28613FECDF FOREIGN KEY (session_id) REFERENCES sessions (id)');
        $this->addSql('ALTER TABLE sessions ADD CONSTRAINT FK_9A609D13BEFD98D1 FOREIGN KEY (training_id) REFERENCES trainings (id)');
        $this->addSql('ALTER TABLE teachings ADD CONSTRAINT FK_3E79ADC4FE54D947 FOREIGN KEY (group_id) REFERENCES `groups` (id)');
        $this->addSql('ALTER TABLE teachings ADD CONSTRAINT FK_3E79ADC4591CC992 FOREIGN KEY (course_id) REFERENCES courses (id)');
        $this->addSql('ALTER TABLE teachings ADD CONSTRAINT FK_3E79ADC47D2D84D5 FOREIGN KEY (professor_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE tests ADD CONSTRAINT FK_1260FC5E899F2BB2 FOREIGN KEY (teaching_id) REFERENCES teachings (id)');
        $this->addSql('ALTER TABLE tests ADD CONSTRAINT FK_1260FC5E61220EA6 FOREIGN KEY (creator_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE trainings ADD CONSTRAINT FK_66DC433032C8A3DE FOREIGN KEY (organization_id) REFERENCES organizations (id)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E932C8A3DE FOREIGN KEY (organization_id) REFERENCES organizations (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE teachings DROP FOREIGN KEY FK_3E79ADC4591CC992');
        $this->addSql('ALTER TABLE groups_students DROP FOREIGN KEY FK_1F7B776DFE54D947');
        $this->addSql('ALTER TABLE teachings DROP FOREIGN KEY FK_3E79ADC4FE54D947');
        $this->addSql('ALTER TABLE trainings DROP FOREIGN KEY FK_66DC433032C8A3DE');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E932C8A3DE');
        $this->addSql('ALTER TABLE paths DROP FOREIGN KEY FK_8BBA8611BC21F71D');
        $this->addSql('ALTER TABLE course_units DROP FOREIGN KEY FK_1B73A13F0B4624');
        $this->addSql('ALTER TABLE `groups` DROP FOREIGN KEY FK_F06D3970D96C566B');
        $this->addSql('ALTER TABLE path_units DROP FOREIGN KEY FK_1CD2DC97D96C566B');
        $this->addSql('ALTER TABLE courses DROP FOREIGN KEY FK_A9A55A4C5E3BC9B6');
        $this->addSql('ALTER TABLE courses DROP FOREIGN KEY FK_A9A55A4C613FECDF');
        $this->addSql('ALTER TABLE `groups` DROP FOREIGN KEY FK_F06D3970613FECDF');
        $this->addSql('ALTER TABLE path_groups DROP FOREIGN KEY FK_91F3A40B613FECDF');
        $this->addSql('ALTER TABLE paths DROP FOREIGN KEY FK_8BBA8611613FECDF');
        $this->addSql('ALTER TABLE session_parts DROP FOREIGN KEY FK_B23D1F28613FECDF');
        $this->addSql('ALTER TABLE tests DROP FOREIGN KEY FK_1260FC5E899F2BB2');
        $this->addSql('ALTER TABLE marks DROP FOREIGN KEY FK_3C6AFA531E5D0459');
        $this->addSql('ALTER TABLE sessions DROP FOREIGN KEY FK_9A609D13BEFD98D1');
        $this->addSql('ALTER TABLE course_units DROP FOREIGN KEY FK_1B73A1BF396750');
        $this->addSql('ALTER TABLE courses DROP FOREIGN KEY FK_A9A55A4CF8BD700D');
        $this->addSql('ALTER TABLE path_units DROP FOREIGN KEY FK_1CD2DC97BF396750');
        $this->addSql('ALTER TABLE paths DROP FOREIGN KEY FK_8BBA8611BF396750');
        $this->addSql('ALTER TABLE groups_students DROP FOREIGN KEY FK_1F7B776DCB944F1A');
        $this->addSql('ALTER TABLE marks DROP FOREIGN KEY FK_3C6AFA53CB944F1A');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('ALTER TABLE teachings DROP FOREIGN KEY FK_3E79ADC47D2D84D5');
        $this->addSql('ALTER TABLE tests DROP FOREIGN KEY FK_1260FC5E61220EA6');
        $this->addSql('DROP TABLE course_units');
        $this->addSql('DROP TABLE courses');
        $this->addSql('DROP TABLE `groups`');
        $this->addSql('DROP TABLE groups_students');
        $this->addSql('DROP TABLE marks');
        $this->addSql('DROP TABLE organizations');
        $this->addSql('DROP TABLE path_groups');
        $this->addSql('DROP TABLE path_units');
        $this->addSql('DROP TABLE paths');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('DROP TABLE session_parts');
        $this->addSql('DROP TABLE sessions');
        $this->addSql('DROP TABLE teachings');
        $this->addSql('DROP TABLE tests');
        $this->addSql('DROP TABLE trainings');
        $this->addSql('DROP TABLE units');
        $this->addSql('DROP TABLE users');
    }
}
