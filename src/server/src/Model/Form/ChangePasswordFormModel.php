<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Model\Form;

use Symfony\Component\Validator\Constraints as Assert;

class ChangePasswordFormModel
{
    /**
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 2,
     *      max = 4096,
     *      minMessage = "Your password should be at least {{ limit }} characters",
     * )
     */
    public ?string $plainPassword;

    /**
     * @Assert\EqualTo(
     *     propertyPath="plainPassword"
     * )
     */
    public ?string $repeatedPlainPassword;

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }
}
