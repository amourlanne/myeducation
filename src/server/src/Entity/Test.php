<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Repository\TestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=TestRepository::class)
 * @ORM\Table(name="tests")
 */
class Test
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"test_read", "teaching_test_read", "test_list", "teaching_test_list", "mark_test_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"test_read", "test_create", "teaching_test_read", "test_list", "teaching_test_list", "mark_test_read", "mark_l_test"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Teaching::class, inversedBy="tests")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"test_read", "test_list"})
     */
    private ?Teaching $teaching;

    /**
     * @ORM\ManyToOne(targetEntity=Professor::class, inversedBy="tests")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"test_read", "teaching_test_read", "test_list", "teaching_test_list"})
     */
    private $creator;

    /**
     * @ORM\OneToMany(targetEntity=Mark::class, mappedBy="test", orphanRemoval=true)
     * @Groups({"test_read"})
     */
    private $marks;

    public function __construct()
    {
        $this->marks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTeaching(): ?Teaching
    {
        return $this->teaching;
    }

    public function setTeaching(?Teaching $teaching): self
    {
        $this->teaching = $teaching;

        return $this;
    }

    public function getCreator(): ?Professor
    {
        return $this->creator;
    }

    public function setCreator(?Professor $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return Collection|Mark[]
     */
    public function getMarks(): Collection
    {
        return $this->marks;
    }

    public function addMark(Mark $mark): self
    {
        if (!$this->marks->contains($mark)) {
            $this->marks[] = $mark;
            $mark->setTest($this);
        }

        return $this;
    }

    public function removeMark(Mark $mark): self
    {
        if ($this->marks->contains($mark)) {
            $this->marks->removeElement($mark);
            // set the owning side to null (unless already changed)
            if ($mark->getTest() === $this) {
                $mark->setTest(null);
            }
        }

        return $this;
    }
}
