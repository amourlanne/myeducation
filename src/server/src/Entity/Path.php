<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Repository\PathRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PathRepository::class)
 * @ORM\Table(name="paths")
 */
class Path extends Unit
{
    /**
     * @Groups({
     *     "path_group_read_path",
     *     "course_read_path",
     *     "course_list_path",
     *     "group_read_path",
     * })
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({
     *     "path_list",
     *     "path_read",
     *     "path_create",
     *     "course_list_path",
     *     "course_read_path",
     *     "group_list_path",
     *     "group_read_path",
     *     "path_group_read_path"
     * })
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Group::class, mappedBy="path")
     */
    private $groups;

    /**
     * @ORM\ManyToOne(targetEntity=Session::class, inversedBy="paths")
     * @ORM\JoinColumn(nullable=false)
     */
    private $session;

//    /**
//     * @ORM\OneToMany(targetEntity=Course::class, mappedBy="path")
//     * @Groups({"path_read"})
//     */
//    private $courses;

    /**
     * @ORM\ManyToOne(targetEntity=PathGroup::class, inversedBy="paths")
     * @ORM\JoinColumn(onDelete="SET NULL")
     * @Groups({"path_read_path_group", "path_list_path_group"})
     */
    private $pathGroup;

    /**
     * @ORM\OneToMany(targetEntity=PathUnit::class, mappedBy="path", orphanRemoval=true)
     */
    private $pathUnits;

    public function __construct()
    {
        parent::__construct();

        $this->groups = new ArrayCollection();
        $this->pathUnits = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Group[]
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function addGroup(Group $group): self
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
            $group->setPath($this);
        }

        return $this;
    }

    public function removeGroup(Group $group): self
    {
        if ($this->groups->contains($group)) {
            $this->groups->removeElement($group);
            // set the owning side to null (unless already changed)
            if ($group->getPath() === $this) {
                $group->setPath(null);
            }
        }

        return $this;
    }

    public function getSession(): ?Session
    {
        return $this->session;
    }

    public function setSession(?Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    public function getPathGroup(): ?PathGroup
    {
        return $this->pathGroup;
    }

    public function setPathGroup(?PathGroup $pathGroup): self
    {
        $this->pathGroup = $pathGroup;

        return $this;
    }

    /**
     * @return Collection|PathUnit[]
     */
    public function getPathUnits(): Collection
    {
        return $this->pathUnits;
    }

    public function addPathUnit(PathUnit $pathUnit): self
    {
        if (!$this->pathUnits->contains($pathUnit)) {
            $this->pathUnits[] = $pathUnit;
            $pathUnit->setPath($this);
        }

        return $this;
    }

    public function removePathUnit(PathUnit $pathUnit): self
    {
        if ($this->pathUnits->contains($pathUnit)) {
            $this->pathUnits->removeElement($pathUnit);
            // set the owning side to null (unless already changed)
            if ($pathUnit->getPath() === $this) {
                $pathUnit->setPath(null);
            }
        }

        return $this;
    }
}
