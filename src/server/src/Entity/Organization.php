<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Repository\OrganizationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=OrganizationRepository::class)
 * @ORM\Table(name="organizations", uniqueConstraints={@ORM\UniqueConstraint(name="organization_slug", columns={"slug"})})
 */
class Organization
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=65)
     * @Groups({"organization_read", "organization_post", "user_read"})
     */
    private string $slug;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"organization_read", "organization_post", "user_read"})
     */
    private string $name;

    /**
     * @ORM\Column(type="organization_sector", length=255)
     * @Groups({"organization_read", "organization_post"})
     */
    private string $sector;

    /**
     * @ORM\OneToMany(targetEntity=OrganizationUser::class, mappedBy="organization", orphanRemoval=true)
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity=Training::class, mappedBy="organization", orphanRemoval=true)
     */
    private $trainings;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->trainings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSector(): ?string
    {
        return $this->sector;
    }

    public function setSector(string $sector): self
    {
        $this->sector = $sector;

        return $this;
    }

    /**
     * @return Collection|OrganizationUser[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addOrganizationUser(OrganizationUser $organizationUser): self
    {
        if (!$this->users->contains($organizationUser)) {
            $this->users[] = $organizationUser;
            $organizationUser->setOrganization($this);
        }

        return $this;
    }

    public function removeOrganizationUser(OrganizationUser $organizationUser): self
    {
        if ($this->users->contains($organizationUser)) {
            $this->users->removeElement($organizationUser);
            // set the owning side to null (unless already changed)
            if ($organizationUser->getOrganization() === $this) {
                $organizationUser->setOrganization(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Training[]
     */
    public function getTrainings(): Collection
    {
        return $this->trainings;
    }

    public function addTraining(Training $training): self
    {
        if (!$this->trainings->contains($training)) {
            $this->trainings[] = $training;
            $training->setOrganization($this);
        }

        return $this;
    }

    public function removeTraining(Training $training): self
    {
        if ($this->trainings->contains($training)) {
            $this->trainings->removeElement($training);
            // set the owning side to null (unless already changed)
            if ($training->getOrganization() === $this) {
                $training->setOrganization(null);
            }
        }

        return $this;
    }
}
