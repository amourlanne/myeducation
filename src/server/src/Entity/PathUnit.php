<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Repository\PathUnitRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PathUnitRepository::class)
 * @ORM\Table(name="path_units")
 */
class PathUnit extends Unit
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Path::class, inversedBy="pathUnits")
     * @ORM\JoinColumn(nullable=false)
     */
    private $path;

    /**
     * @ORM\OneToMany(targetEntity=CourseUnit::class, mappedBy="pathUnit", orphanRemoval=true)
     */
    private $courseUnits;

    public function __construct()
    {
        parent::__construct();

        $this->courseUnits = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPath(): ?Path
    {
        return $this->path;
    }

    public function setPath(?Path $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return Collection|CourseUnit[]
     */
    public function getCourseUnits(): Collection
    {
        return $this->courseUnits;
    }

    public function addCourseUnit(CourseUnit $courseUnit): self
    {
        if (!$this->courseUnits->contains($courseUnit)) {
            $this->courseUnits[] = $courseUnit;
            $courseUnit->setPathUnit($this);
        }

        return $this;
    }

    public function removeCourseUnit(CourseUnit $courseUnit): self
    {
        if ($this->courseUnits->contains($courseUnit)) {
            $this->courseUnits->removeElement($courseUnit);
            // set the owning side to null (unless already changed)
            if ($courseUnit->getPathUnit() === $this) {
                $courseUnit->setPathUnit(null);
            }
        }

        return $this;
    }
}
