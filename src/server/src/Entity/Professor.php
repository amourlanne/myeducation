<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Repository\ProfessorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProfessorRepository::class)
 */
class Professor extends OrganizationUser
{
    /**
     * @ORM\OneToMany(targetEntity=Teaching::class, mappedBy="professor")
     */
    private $teachings;

    /**
     * @ORM\OneToMany(targetEntity=Test::class, mappedBy="creator")
     */
    private $tests;

    public function __construct()
    {
        parent::__construct();
        $this->teachings = new ArrayCollection();
        $this->tests = new ArrayCollection();
    }

    public function getType()
    {
        return 'professor';
    }

    /**
     * @return Collection|Teaching[]
     */
    public function getTeachings(): Collection
    {
        return $this->teachings;
    }

    public function addTeaching(Teaching $teaching): self
    {
        if (!$this->teachings->contains($teaching)) {
            $this->teachings[] = $teaching;
            $teaching->setProfessor($this);
        }

        return $this;
    }

    public function removeTeaching(Teaching $teaching): self
    {
        if ($this->teachings->contains($teaching)) {
            $this->teachings->removeElement($teaching);
            // set the owning side to null (unless already changed)
            if ($teaching->getProfessor() === $this) {
                $teaching->setProfessor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Test[]
     */
    public function getTests(): Collection
    {
        return $this->tests;
    }

    public function addTest(Test $test): self
    {
        if (!$this->tests->contains($test)) {
            $this->tests[] = $test;
            $test->setCreator($this);
        }

        return $this;
    }

    public function removeTest(Test $test): self
    {
        if ($this->tests->contains($test)) {
            $this->tests->removeElement($test);
            // set the owning side to null (unless already changed)
            if ($test->getCreator() === $this) {
                $test->setCreator(null);
            }
        }

        return $this;
    }
}
