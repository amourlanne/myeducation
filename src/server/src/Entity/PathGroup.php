<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;

use App\Repository\PathGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass=PathGroupRepository::class)
 * @ORM\Table(name="path_groups")
 */
class PathGroup
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"path_group_list", "path_group_read", "path_list_path_group", "path_read_path_group"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Groups({"path_group_list", "path_group_read", "path_group_create", "path_list_path_group"})
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     * @Serializer\Groups({"path_group_read", "path_group_create"})
     */
    private $uniqueChoice;

    /**
     * @ORM\Column(type="boolean")
     * @Serializer\Groups({"path_group_read", "path_group_create"})
     */
    private $mandatoryChoice;

    /**
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"path_group_read", "path_group_create"})
     */
    private $maxChoice;

    /**
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"path_group_read", "path_group_create"})
     */
    private $minChoice;

    /**
     * @ORM\OneToMany(targetEntity=Path::class, mappedBy="pathGroup")
     * @Serializer\Groups({"path_group_read", "path_group_read_path"})
     */
    private $paths;

    /**
     * @ORM\ManyToOne(targetEntity=Session::class, inversedBy="pathGroups")
     * @ORM\JoinColumn(nullable=false)
     */
    private $session;

    public function __construct()
    {
        $this->paths = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUniqueChoice(): ?bool
    {
        return $this->uniqueChoice;
    }

    public function setUniqueChoice(bool $uniqueChoice): self
    {
        $this->uniqueChoice = $uniqueChoice;

        return $this;
    }

    public function getMandatoryChoice(): ?bool
    {
        return $this->mandatoryChoice;
    }

    public function setMandatoryChoice(bool $mandatoryChoice): self
    {
        $this->mandatoryChoice = $mandatoryChoice;

        return $this;
    }

    public function getMaxChoice(): ?int
    {
        return $this->maxChoice;
    }

    public function setMaxChoice(int $maxChoice): self
    {
        $this->maxChoice = $maxChoice;

        return $this;
    }

    public function getMinChoice(): ?int
    {
        return $this->minChoice;
    }

    public function setMinChoice(int $minChoice): self
    {
        $this->minChoice = $minChoice;

        return $this;
    }

    /**
     * @return Collection|Path[]
     */
    public function getPaths(): Collection
    {
        return $this->paths;
    }

    public function addPath(Path $path): self
    {
        if (!$this->paths->contains($path)) {
            $this->paths[] = $path;
            $path->setPathGroup($this);
        }

        return $this;
    }

    public function removePath(Path $path): self
    {
        if ($this->paths->contains($path)) {
            $this->paths->removeElement($path);
            // set the owning side to null (unless already changed)
            if ($path->getPathGroup() === $this) {
                $path->setPathGroup(null);
            }
        }

        return $this;
    }

    public function getSession(): ?Session
    {
        return $this->session;
    }

    public function setSession(?Session $session): self
    {
        $this->session = $session;

        return $this;
    }
}
