<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Security;

use Symfony\Component\HttpFoundation\Request;

class OrganizationAdminRequestMatcher extends OrganizationSlugHeaderRequestMatcher
{
    public function matches(Request $request)
    {
        preg_match('/^\/organization-admin\/*/', $request->getRequestUri(), $matches);

        return parent::matches($request) && isset($matches[0]);
    }
}
