<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Security\Provider;

use App\Entity\OrganizationUser;
use App\Entity\User;
use App\Repository\OrganizationUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class OrganizationUserProvider implements UserProviderInterface
{
    private EntityManagerInterface $entityManager;
    private RequestStack $requestStack;

    public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack)
    {
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
    }

    public function getUserClass(): string
    {
        return OrganizationUser::class;
    }

    public function loadUserByUsername(string $username)
    {
        $request = $this->requestStack->getCurrentRequest();

        $organizationSlug = $request->headers->get('X-Organization-Slug');

        if (!$organizationSlug) {
            throw new UsernameNotFoundException();
        }

        $organization = $request->attributes->get('organization');

        if (!$organization) {
            throw new UsernameNotFoundException();
        }

        /** @var OrganizationUserRepository $organizationUserRepository */
        $organizationUserRepository = $this->entityManager
            ->getRepository($this->getUserClass());

        $organizationUser = $organizationUserRepository
            ->findOneBy([
                'email' => $username,
                'organization' => $organization,
            ]);

        if (!$organizationUser) {
            throw new UsernameNotFoundException();
        }

        return $organizationUser;
    }

    public function refreshUser(UserInterface $user)
    {
        /** @var User $user */
        $request = $this->requestStack->getCurrentRequest();
        $organization = $request->attributes->get('organization');

        $refreshedUser = $this->entityManager
            ->getRepository(OrganizationUser::class)
            ->findOneBy([
                'email' => $user->getEmail(),
                'organization' => $organization,
            ]);

        if (!$refreshedUser) {
            throw new UsernameNotFoundException(sprintf('User with email "%s" could not be reloaded.', $user->getEmail()));
        }

        return $refreshedUser;
    }

    public function supportsClass(string $class)
    {
        return User::class === $class || is_subclass_of($class, User::class);
    }
}
