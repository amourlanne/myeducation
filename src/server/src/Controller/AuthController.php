<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\SerializerInterface;

class AuthController extends AbstractController
{
    private Security $security;
    private SerializerInterface $serializer;

    public function __construct(Security $security, SerializerInterface $serializer)
    {
        $this->security = $security;
        $this->serializer = $serializer;
    }

    /**
     * @Route(
     *     "/authenticate",
     *     name="authenticate",
     *     methods={"GET"}
     * )
     */
    public function authenticate(): JsonResponse
    {
        $user = $this->security->getUser();

        $userData = $this->serializer->serialize($user, 'json', ['groups' => 'user_read']);

        return new JsonResponse($userData, 200, [], true);
    }
}
