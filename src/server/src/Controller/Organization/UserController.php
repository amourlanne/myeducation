<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Organization;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class UserController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @Route(
     *     "/users",
     *     name="get_organization_users",
     *     methods={"GET"},
     *     condition="request.headers.get('X-Organization-Slug')"
     * )
     */
    public function getOrganizationUsers(Request $request): JsonResponse
    {
        $organization = $request->attributes->get('organization');

        $organizationUsersData = $this->serializer
            ->serialize($organization->getUsers(), 'json', ['groups' => 'user_read']);

        return new JsonResponse($organizationUsersData, 200, [], true);
    }
}
