<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Organization\Admin\User;

use App\Entity\Organization;
use App\Entity\OrganizationAdmin;
use App\Entity\Professor;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Annotations as OpenApi;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class OrganizationUserController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @OpenApi\Tag(name="Organization User")
     * @Route(
     *     "/organization-admin/professors",
     *     name="organization_admin_get_organization_professors",
     *     methods={"GET"}
     * )
     */
    public function getOrganizationProfessors(Organization $organization): JsonResponse
    {
        $studentsOrganization = $this->entityManager
            ->getRepository(Professor::class)
            ->findBy(['organization' => $organization]);

        $studentsOrganizationData = $this->serializer->serialize($studentsOrganization, 'json', ['groups' => 'user_read']);

        return new JsonResponse($studentsOrganizationData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization User")
     * @Route(
     *     "/organization-admin/admins",
     *     name="organization_admin_get_organization_admins",
     *     methods={"GET"}
     * )
     */
    public function getOrganizationAdmins(Organization $organization): JsonResponse
    {
        $studentsOrganization = $this->entityManager
            ->getRepository(OrganizationAdmin::class)
            ->findBy(['organization' => $organization]);

        $studentsOrganizationData = $this->serializer->serialize($studentsOrganization, 'json', ['groups' => 'user_read']);

        return new JsonResponse($studentsOrganizationData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization User")
     * @Route(
     *     "/organization-admin/admins",
     *     name="organization_admin_create_organization_admin",
     *     methods={"POST"}
     * )
     */
    public function createOrganizationAdmin(Request $request, Organization $organization): JsonResponse
    {
        /** @var OrganizationAdmin $organizationAdmin */
        $organizationAdmin = $this->serializer
            ->deserialize($request->getContent(), OrganizationAdmin::class, 'json', ['groups' => ['user_create']]);

        $organizationAdmin->setOrganization($organization);

        $this->entityManager->persist($organizationAdmin);
        $this->entityManager->flush();

        $organisationAdminData = $this->serializer->serialize($organizationAdmin, 'json', ['groups' => ['user_read']]);

        return new JsonResponse($organisationAdminData, 201, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization User")
     * @Route(
     *     "/organization-admin/admins/{user_email}",
     *     name="organization_admin_get_organization_admin",
     *     methods={"GET"}
     * )
     * @ParamConverter("organizationAdmin", options={"mapping": {"user_email": "email", "organization": "organization"}})
     */
    public function getOrganizationAdmin(Organization $organization, OrganizationAdmin $organizationAdmin): JsonResponse
    {
        $organizationAdminData = $this->serializer->serialize($organizationAdmin, 'json', ['groups' => 'user_read']);

        return new JsonResponse($organizationAdminData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization User")
     * @Route(
     *     "/organization-admin/admins/{user_email}",
     *     name="organization_admin_update_organization_admin",
     *     methods={"PUT"}
     * )
     * @ParamConverter("organizationAdmin", options={"mapping": {"user_email": "email", "organization": "organization"}})
     */
    public function updateOrganizationAdmin(Request $request, Organization $organization, OrganizationAdmin $organizationAdmin): JsonResponse
    {
        $organizationAdmin = $this->serializer->deserialize(
            $request->getContent(),
            OrganizationAdmin::class,
            'json',
            [
                'groups' => ['user_create'],
                'object_to_populate' => $organizationAdmin,
            ]);

        $this->entityManager->persist($organizationAdmin);
        $this->entityManager->flush();

        $organisationAdminData = $this->serializer->serialize($organizationAdmin, 'json', ['groups' => ['user_read']]);

        return new JsonResponse($organisationAdminData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization User")
     * @Route(
     *     "/organization-admin/admins/{user_email}",
     *     name="organization_admin_delete_organization_admin",
     *     methods={"DELETE"}
     * )
     * @ParamConverter("organizationAdmin", options={"mapping": {"user_email": "email", "organization": "organization"}})
     */
    public function deleteOrganizationAdmin(Organization $organization, OrganizationAdmin $organizationAdmin): JsonResponse
    {
        $this->entityManager->remove($organizationAdmin);
        $this->entityManager->flush();

        return new JsonResponse('', 204, [], true);
    }
}
