<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Organization\Admin\Training\Path;

use App\Entity\Organization;
use App\Entity\Path;
use App\Entity\PathGroup;
use App\Entity\Session;
use App\Entity\Training;
use App\Repository\PathRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use OpenApi\Annotations as OpenApi;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class OrganizationTrainingPathController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @OpenApi\Tag(name="Organization Training Path")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/paths",
     *     name="organization_admin_get_organization_training_path_list",
     *     methods={"GET"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     */
    public function getOrganizationTrainingPathList(Request $request, Organization $organization, Training $training, Session $session): JsonResponse
    {
        /** @var PathRepository $pathRepository */
        $pathRepository = $this->entityManager
        ->getRepository(Path::class);

        $pathListQueryBuilder = $pathRepository
            ->createQueryBuilder('p')
            ->where('p.session = :sessionParam')
            ->setParameter(':sessionParam', $session);

        $pathListQuery = $pathListQueryBuilder
            ->getQuery();

        $page = (int) $request->query->get('page', '1');
        $limit = (int) $request->query->get('limit', '10');

        $pathListQuery
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit);

        $pathListPaginator = new Paginator($pathListQuery);

        $totalItems = $pathListPaginator->count();
        $pages = 0 === $totalItems ?
            1 :
            ceil($totalItems / $limit);

        $pathListPaginatorData = [
            'items' => $pathListPaginator,
            'totalItems' => $totalItems,
            'pages' => $pages,
        ];

        $pathListData = $this->serializer->serialize($pathListPaginatorData, 'json', ['groups' => ['path_list', 'unit_list', 'path_list_path_group']]);

        return new JsonResponse($pathListData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Path")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/paths/{path_id}",
     *     name="organization_admin_get_organization_training_path",
     *     methods={"GET"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("path", options={"mapping": {"path_id": "id", "session": "session"}})
     */
    public function getOrganizationTrainingPath(Organization $organization, Training $training, Session $session, Path $path): JsonResponse
    {
        $pathData = $this->serializer->serialize($path, 'json', ['groups' => ['path_read', 'unit_read', 'path_read_path_group']]);

        return new JsonResponse($pathData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Path")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/paths",
     *     name="organization_admin_create_organization_training_path",
     *     methods={"POST"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     */
    public function createOrganizationTrainingPath(Request $request, Organization $organization, Training $training, Session $session): JsonResponse
    {
        $content = $request->getContent();
        $contentObject = json_decode($content);

        /** @var Path $path */
        $path = $this->serializer->deserialize(
            $content,
            Path::class,
            'json',
            [
                'groups' => ['path_create'],
            ]);

        if (property_exists($contentObject, 'pathGroup')) {
            switch (gettype($contentObject->pathGroup)) {
                case 'NULL':
                    $path->setPathGroup(null);
                    break;
                case 'integer':
                    $pathGroupId = $contentObject->pathGroup;

                    /** @var PathGroup|null $pathGroup */
                    $pathGroup = $this->entityManager
                        ->getRepository(PathGroup::class)
                        ->findOneBy([
                            'id' => $pathGroupId,
                            'session' => $session,
                        ]);

                    if (null === $pathGroup) {
                        throw new NotFoundHttpException(sprintf('No pathGroup found for id %s', $pathGroupId));
                    }

                    $path->setPathGroup($pathGroup);
                    break;
                default:
                    throw new BadRequestHttpException('Wrong pathGroup id.');
            }
        }

        $path->setSession($session);

        $this->entityManager->persist($path);
        $this->entityManager->flush();

        $pathData = $this->serializer->serialize($path, 'json', ['groups' => ['path_read', 'unit_read', 'path_read_path_group']]);

        return new JsonResponse($pathData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Path")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/paths/{path_id}",
     *     name="organization_admin_delete_organization_training_path",
     *     methods={"DELETE"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("path", options={"mapping": {"path_id": "id", "session": "session"}})
     */
    public function deleteOrganizationTrainingPath(Request $request, Organization $organization, Training $training, Session $session, Path $path): JsonResponse
    {
        $this->entityManager->remove($path);
        $this->entityManager->flush();

        return new JsonResponse('', 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Path")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/paths/{path_id}",
     *     name="organization_admin_update_organization_training_path",
     *     methods={"PUT"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("path", options={"mapping": {"path_id": "id", "session": "session"}})
     */
    public function updateOrganizationTrainingPath(Request $request, Organization $organization, Training $training, Session $session, Path $path): JsonResponse
    {
        $content = $request->getContent();
        $contentObject = json_decode($content);

        /** @var Path $path */
        $path = $this->serializer->deserialize(
            $content,
            Path::class,
            'json',
            [
                'groups' => ['path_create'],
                'object_to_populate' => $path,
            ]);

        if (property_exists($contentObject, 'pathGroup')) {
            switch (gettype($contentObject->pathGroup)) {
                case 'NULL':
                    $path->setPathGroup(null);
                    break;
                case 'integer':
                    $pathGroupId = $contentObject->pathGroup;

                    /** @var PathGroup|null $pathGroup */
                    $pathGroup = $this->entityManager
                        ->getRepository(PathGroup::class)
                        ->findOneBy([
                            'id' => $pathGroupId,
                            'session' => $session,
                        ]);

                    if (null === $pathGroup) {
                        throw new NotFoundHttpException(sprintf('No pathGroup found for id %s', $pathGroupId));
                    }

                    $path->setPathGroup($pathGroup);
                    break;
                default:
                    throw new BadRequestHttpException('Wrong pathGroup id.');
            }
        }

        $this->entityManager->persist($path);
        $this->entityManager->flush();

        $pathData = $this->serializer->serialize($path, 'json', ['groups' => ['path_read', 'unit_read', 'path_read_path_group']]);

        return new JsonResponse($pathData, 200, [], true);
    }
}
