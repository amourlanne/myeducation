<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Organization\Admin\Training\PathGroup;

use App\Entity\Organization;
use App\Entity\Path;
use App\Entity\PathGroup;
use App\Entity\Session;
use App\Entity\Training;
use App\Repository\PathGroupRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use OpenApi\Annotations as OpenApi;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class OrganizationTrainingPathGroupController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @OpenApi\Tag(name="Organization Training PathGroup")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/path-groups",
     *     name="organization_admin_get_organization_training_path_group_list",
     *     methods={"GET"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     */
    public function getOrganizationTrainingPathGroupList(Request $request, Organization $organization, Training $training, Session $session): JsonResponse
    {
        /** @var PathGroupRepository $pathGroupRepository */
        $pathGroupRepository = $this->entityManager
        ->getRepository(PathGroup::class);

        $pathGroupListQueryBuilder = $pathGroupRepository
            ->createQueryBuilder('pg')
            ->where('pg.session = :sessionParam')
            ->setParameter(':sessionParam', $session);

        $pathGroupListQuery = $pathGroupListQueryBuilder
            ->getQuery();

        $page = (int) $request->query->get('page', '1');
        $limit = (int) $request->query->get('limit', '10');

        $pathGroupListQuery
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit);

        $pathGroupListPaginator = new Paginator($pathGroupListQuery);

        $totalItems = $pathGroupListPaginator->count();
        $pages = 0 === $totalItems ?
            1 :
            ceil($totalItems / $limit);

        $pathGroupListPaginatorData = [
            'items' => $pathGroupListPaginator,
            'totalItems' => $totalItems,
            'pages' => $pages,
        ];

        $pathGroupListData = $this->serializer->serialize($pathGroupListPaginatorData, 'json', ['groups' => 'path_group_list']);

        return new JsonResponse($pathGroupListData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training PathGroup")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/path-groups/{path_group_id}",
     *     name="organization_admin_get_organization_training_path_group",
     *     methods={"GET"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("pathGroup", options={"mapping": {"path_group_id": "id", "session": "session"}})
     */
    public function getOrganizationTrainingPathGroup(Organization $organization, Training $training, Session $session, PathGroup $pathGroup): JsonResponse
    {
        $pathGroupData = $this->serializer->serialize($pathGroup, 'json', ['groups' => ['path_group_read', 'path_group_read_path']]);

        return new JsonResponse($pathGroupData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Path")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/path-groups",
     *     name="organization_admin_create_organization_training_path_group",
     *     methods={"POST"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     */
    public function createOrganizationTrainingPathGroup(Request $request, Organization $organization, Training $training, Session $session): JsonResponse
    {
        /** @var PathGroup $pathGroup */
        $pathGroup = $this->serializer->deserialize(
            $request->getContent(),
            PathGroup::class,
            'json',
            [
                'groups' => ['path_group_create'],
            ]);

        $pathGroup->setSession($session);

        $this->entityManager->persist($pathGroup);
        $this->entityManager->flush();

        $pathGroupData = $this->serializer->serialize($pathGroup, 'json', ['groups' => ['path_group_read']]);

        return new JsonResponse($pathGroupData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training PathGroup")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/path-groups/{path_group_id}",
     *     name="organization_admin_delete_organization_training_path_group",
     *     methods={"DELETE"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("pathGroup", options={"mapping": {"path_group_id": "id", "session": "session"}})
     */
    public function deleteOrganizationTrainingPathGroup(Request $request, Organization $organization, Training $training, Session $session, PathGroup $pathGroup): JsonResponse
    {
        $this->entityManager->remove($pathGroup);
        $this->entityManager->flush();

        return new JsonResponse('', 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Path")
     * @Route(
     *     "/organization-admin/trainings/{training_slug}/{school_year}/path-groups/{path_group_id}",
     *     name="organization_admin_update_organization_training_path_group",
     *     methods={"PUT"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("pathGroup", options={"mapping": {"path_group_id": "id", "session": "session"}})
     */
    public function updateOrganizationTrainingPathGroup(Request $request, Organization $organization, Training $training, Session $session, PathGroup $pathGroup): JsonResponse
    {
        $pathGroup = $this->serializer->deserialize(
            $request->getContent(),
            PathGroup::class,
            'json',
            [
                'groups' => ['path_group_create'],
                'object_to_populate' => $pathGroup,
            ]);

        $this->entityManager->persist($pathGroup);
        $this->entityManager->flush();

        $pathGroupData = $this->serializer->serialize($pathGroup, 'json', ['groups' => ['path_group_read', 'path_group_read_path']]);

        return new JsonResponse($pathGroupData, 200, [], true);
    }
}
