<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin\Organization\Training\Test;

use App\Entity\Mark;
use App\Entity\Organization;
use App\Entity\Session;
use App\Entity\Test;
use App\Entity\Training;
use App\Repository\MarkRepository;
use App\Repository\TestRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use OpenApi\Annotations as OpenApi;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class OrganizationTrainingTestController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @OpenApi\Tag(name="Organization Training Test")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/tests",
     *     name="admin_get_organization_training_test_list",
     *     methods={"GET"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     */
    public function getOrganizationTrainingTestList(Request $request, Organization $organization, Training $training, Session $session): JsonResponse
    {
        /** @var TestRepository $testRepository */
        $testRepository = $this->entityManager
            ->getRepository(Test::class);

        $testListQueryBuilder = $testRepository
            ->createQueryBuilder('t')
            ->innerJoin('t.teaching', 'tch')
            ->innerJoin('tch.course', 'c')
            ->where('c.session = :sessionParam')
            ->setParameter(':sessionParam', $session);

        $testListQuery = $testListQueryBuilder
            ->getQuery();

        $page = (int) $request->query->get('page', '1');
        $limit = (int) $request->query->get('limit', '10');

        $testListQuery
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit);

        $testListPaginator = new Paginator($testListQuery);

        $totalItems = $testListPaginator->count();
        $pages = 0 === $totalItems ?
            1 :
            ceil($totalItems / $limit);

        $testListPaginatorData = [
            'items' => $testListPaginator,
            'totalItems' => $totalItems,
            'pages' => $pages,
        ];

        $testListData = $this->serializer->serialize($testListPaginatorData, 'json', ['groups' => ['test_list']]);

        return new JsonResponse($testListData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Test")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/tests/{test_id}",
     *     name="admin_get_organization_training_test",
     *     methods={"GET"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     */
    public function getOrganizationTrainingTest(Request $request, Organization $organization, Training $training, Session $session, string $test_id): JsonResponse
    {
        /** @var TestRepository $testRepository */
        $testRepository = $this->entityManager
            ->getRepository(Test::class);

        $test = $testRepository
            ->createQueryBuilder('t')
            ->where('t.id = :testId')
            ->innerJoin('t.teaching', 'tc')
            ->innerJoin('tc.course', 'c')
            ->andWhere('c.session = :sessionParam')
            ->setParameter(':testId', $test_id)
            ->setParameter(':sessionParam', $session)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$test) {
            throw new NotFoundHttpException('No test found for id '.$test_id);
        }

        $testData = $this->serializer->serialize($test, 'json', ['groups' => ['test_read']]);

        return new JsonResponse($testData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Test Mark")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/tests/{test_id}/marks/{mark_id}",
     *     name="admin_get_organization_training_test_mark",
     *     methods={"GET"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     */
    public function getOrganizationTrainingTestMark(Organization $organization, Training $training, Session $session, string $test_id, string $mark_id): JsonResponse
    {
        /** @var MarkRepository $markRepository */
        $markRepository = $this->entityManager
            ->getRepository(Mark::class);

        $mark = $markRepository
            ->createQueryBuilder('m')
            ->where('m.id = :markId')
            ->innerJoin('m.test', 't')
            ->andWhere('t.id = :testId')
            ->innerJoin('t.teaching', 'tc')
            ->innerJoin('tc.course', 'c')
            ->andWhere('c.session = :sessionParam')
            ->setParameter(':markId', $mark_id)
            ->setParameter(':testId', $test_id)
            ->setParameter(':sessionParam', $session)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$mark) {
            throw new NotFoundHttpException('No mark found for id '.$test_id);
        }

        $markData = $this->serializer->serialize($mark, 'json', ['groups' => ['mark_read']]);

        return new JsonResponse($markData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Test Mark")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/tests/{test_id}/marks/{mark_id}",
     *     name="admin_update_organization_training_test_mark",
     *     methods={"PUT"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     */
    public function updateOrganizationTrainingTestMark(Request $request, Organization $organization, Training $training, Session $session, string $test_id, string $mark_id): JsonResponse
    {
        /** @var MarkRepository $markRepository */
        $markRepository = $this->entityManager
            ->getRepository(Mark::class);

        $mark = $markRepository
            ->createQueryBuilder('m')
            ->where('m.id = :markId')
            ->innerJoin('m.test', 't')
            ->andWhere('t.id = :testId')
            ->innerJoin('t.teaching', 'tc')
            ->innerJoin('tc.course', 'c')
            ->andWhere('c.session = :sessionParam')
            ->setParameter(':markId', $mark_id)
            ->setParameter(':testId', $test_id)
            ->setParameter(':sessionParam', $session)
            ->getQuery()
            ->getOneOrNullResult();

        $mark = $this->serializer->deserialize(
            $request->getContent(),
            Mark::class,
            'json',
            [
                'groups' => ['mark_update'],
                'object_to_populate' => $mark,
            ]);

        $this->entityManager->persist($mark);
        $this->entityManager->flush();

        $markData = $this->serializer->serialize($mark, 'json', ['groups' => ['mark_read']]);

        return new JsonResponse($markData, 200, [], true);
    }
}
