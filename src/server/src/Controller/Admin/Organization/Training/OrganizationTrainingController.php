<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin\Organization\Training;

use App\Entity\Organization;
use App\Entity\Session;
use App\Entity\Training;
use App\Repository\TrainingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use OpenApi\Annotations as OpenApi;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class OrganizationTrainingController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @OpenApi\Tag(name="Organization Training")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings",
     *     name="admin_get_organization_trainings",
     *     methods={"GET"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     */
    public function getOrganizationTrainings(Request $request, Organization $organization): JsonResponse
    {
        /** @var TrainingRepository $trainingRepository */
        $trainingRepository = $this->entityManager
            ->getRepository(Training::class);

        $organizationTrainingsQueryBuilder = $trainingRepository
            ->createQueryBuilder('t')
            ->where('t.organization = :organizationParam')
            ->setParameter(':organizationParam', $organization);

        $organizationTrainingsQuery = $organizationTrainingsQueryBuilder
            ->getQuery();

        $page = (int) $request->query->get('page', '1');
        $limit = (int) $request->query->get('limit', '10');

        $organizationTrainingsQuery
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit);

        $organizationTrainingsPaginator = new Paginator($organizationTrainingsQuery);

        $totalItems = $organizationTrainingsPaginator->count();
        $pages = 0 === $totalItems ?
            1 :
            ceil($totalItems / $limit);

        $organizationTrainingsPaginatorData = [
            'items' => $organizationTrainingsPaginator,
            'totalItems' => $totalItems,
            'pages' => $pages,
        ];

        $trainingsOrganizationData = $this->serializer->serialize($organizationTrainingsPaginatorData, 'json', ['groups' => 'training_read']);

        return new JsonResponse($trainingsOrganizationData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}",
     *     name="admin_get_organization_training",
     *     methods={"GET"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     */
    public function getOrganizationTraining(Organization $organization, Training $training): JsonResponse
    {
        $trainingData = $this->serializer->serialize($training, 'json', ['groups' => ['training_read', 'training_session']]);

        return new JsonResponse($trainingData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Session")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}",
     *     name="admin_create_organization_training_session",
     *     methods={"POST"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     */
    public function createOrganizationTrainingSession(Organization $organization, Training $training, string $school_year): JsonResponse
    {
        $session = new Session();
        $session->setTraining($training);
        $session->setSchoolYear($school_year);

        $this->entityManager->persist($session);
        $this->entityManager->flush();

        $sessionData = $this->serializer->serialize($session, 'json', ['groups' => ['session_create']]);

        return new JsonResponse($sessionData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Session")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}",
     *     name="admin_get_organization_training_session",
     *     methods={"GET"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     */
    public function getOrganizationTrainingSession(Organization $organization, Training $training, Session $session): JsonResponse
    {
        $sessionData = $this->serializer->serialize($session, 'json', ['groups' => ['session_read']]);

        return new JsonResponse($sessionData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Session")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}",
     *     name="admin_update_organization_training_session",
     *     methods={"PUT"},
     *     requirements={"school_year"="^(19|20)\d{2}-(19|20)\d{2}$"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     */
    public function updateOrganizationTrainingSession(Request $request, Organization $organization, Training $training, Session $session): JsonResponse
    {
        /** @var Session $session */
        $session = $this->serializer->deserialize(
            $request->getContent(),
            Session::class,
            'json',
            [
                'groups' => ['session_create'],
                'object_to_populate' => $session,
            ]);

        $this->entityManager->persist($session);
        $this->entityManager->flush();

        $sessionData = $this->serializer->serialize($session, 'json', ['groups' => ['session_read']]);

        return new JsonResponse($sessionData, 200, [], true);
    }
}
