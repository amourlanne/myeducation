<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin\Organization\Training\Teaching;

use App\Entity\Course;
use App\Entity\Organization;
use App\Entity\Path;
use App\Entity\Session;
use App\Entity\Teaching;
use App\Entity\Test;
use App\Entity\Training;
use App\Repository\TeachingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use OpenApi\Annotations as OpenApi;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class OrganizationTrainingTeachingController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @OpenApi\Tag(name="Organization Training Teaching")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/teachings",
     *     name="admin_get_organization_training_teaching_list",
     *     methods={"GET"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     */
    public function getOrganizationTrainingTeachingList(Request $request, Organization $organization, Training $training, Session $session): JsonResponse
    {
        /** @var TeachingRepository $teachingRepository */
        $teachingRepository = $this->entityManager
            ->getRepository(Teaching::class);

        $teachingListQueryBuilder = $teachingRepository
            ->createQueryBuilder('t')
            ->innerJoin('t.course', 'c')
            ->where('c.session = :sessionParam')
            ->setParameter(':sessionParam', $session);

        $teachingListQuery = $teachingListQueryBuilder
            ->getQuery();

        $page = (int) $request->query->get('page', '1');
        $limit = (int) $request->query->get('limit', '10');

        $teachingListQuery
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit);

        $teachingListPaginator = new Paginator($teachingListQuery);

        $totalItems = $teachingListPaginator->count();
        $pages = 0 === $totalItems ?
            1 :
            ceil($totalItems / $limit);

        $teachingListPaginatorData = [
            'items' => $teachingListPaginator,
            'totalItems' => $totalItems,
            'pages' => $pages,
        ];

        $teachingListData = $this->serializer->serialize($teachingListPaginatorData, 'json', ['groups' => ['teaching_list', 'user_list']]);

        return new JsonResponse($teachingListData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Teaching")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/teachings/{teaching_id}",
     *     name="admin_get_organization_training_teaching",
     *     methods={"GET"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("teaching", options={"mapping": {"teaching_id": "id", "session": "session"}})
     */
    public function getOrganizationTrainingTeaching(Organization $organization, Training $training, Session $session, Teaching $teaching): JsonResponse
    {
        $teachingData = $this->serializer->serialize($teaching, 'json', ['groups' => ['teaching_list', 'user_list']]);

        return new JsonResponse($teachingData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Teaching")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/teachings/{teaching_id}/tests",
     *     name="admin_get_organization_training_teaching_test_list",
     *     methods={"GET"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("teaching", options={"mapping": {"teaching_id": "id"}})
     */
    public function getOrganizationTrainingTeachingTestList(Organization $organization, Training $training, Session $session, Teaching $teaching): JsonResponse
    {
        // TODO: check if teaching is in this session
        $testListData = $this->serializer->serialize($teaching->getTests(), 'json', ['groups' => ['teaching_test_list']]);

        return new JsonResponse($testListData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Teaching")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/teachings/{teaching_id}/tests/{test_id}",
     *     name="admin_get_organization_training_teaching_test",
     *     methods={"GET"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("teaching", options={"mapping": {"teaching_id": "id"}})
     * @ParamConverter("test", options={"mapping": {"test_id": "id", "teaching": "teaching"}})
     */
    public function getOrganizationTrainingTeachingTest(Organization $organization, Training $training, Session $session, Teaching $teaching, Test $test): JsonResponse
    {
        $testData = $this->serializer->serialize($test, 'json', ['groups' => ['teaching_test_read']]);

        return new JsonResponse($testData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Teaching")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/teachings/{teaching_id}/tests/{test_id}",
     *     name="admin_delete_organization_training_teaching_test",
     *     methods={"DELETE"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("teaching", options={"mapping": {"teaching_id": "id"}})
     * @ParamConverter("test", options={"mapping": {"test_id": "id", "teaching": "teaching"}})
     */
    public function deleteOrganizationTrainingTeachingTest(Organization $organization, Training $training, Session $session, Teaching $teaching, Test $test): JsonResponse
    {
        $this->entityManager->remove($test);
        $this->entityManager->flush();

        return new JsonResponse('', 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Teaching Test")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/teachings/{teaching_id}/tests",
     *     name="admin_create_organization_training_teaching_test",
     *     methods={"POST"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("teaching", options={"mapping": {"teaching_id": "id"}})
     */
    public function createOrganizationTrainingCourseTeachingTest(Request $request, Organization $organization, Training $training, Session $session, Teaching $teaching): JsonResponse
    {
        /** @var Test $test */
        $test = $this->serializer->deserialize(
            $request->getContent(),
            Test::class,
            'json',
            [
                'groups' => ['test_create'],
            ]);

        $test->setTeaching($teaching);

        $this->entityManager->persist($test);
        $this->entityManager->flush();

        $testData = $this->serializer->serialize($test, 'json', ['groups' => ['teaching_test_read']]);

        return new JsonResponse($testData, 200, [], true);
    }

    /**
     * @OpenApi\Tag(name="Organization Training Teaching Test")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/teachings/{teaching_id}/tests/{test_id}",
     *     name="admin_update_organization_training_teaching_test",
     *     methods={"PUT"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     * @ParamConverter("teaching", options={"mapping": {"teaching_id": "id"}})
     * @ParamConverter("test", options={"mapping": {"test_id": "id", "teaching": "teaching"}})
     */
    public function updateOrganizationTrainingCourseTeachingTest(Request $request, Organization $organization, Training $training, Session $session, Teaching $teaching, Test $test): JsonResponse
    {
        $test = $this->serializer->deserialize(
            $request->getContent(),
            Test::class,
            'json',
            [
                'groups' => ['test_create'],
                'object_to_populate' => $test,
            ]);

        $this->entityManager->persist($test);
        $this->entityManager->flush();

        $testData = $this->serializer->serialize($test, 'json', ['groups' => ['teaching_test_read']]);

        return new JsonResponse($testData, 200, [], true);
    }

//
//    /**
//     * @OpenApi\Tag(name="Organization Training Course")
//     * @Route(
//     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/courses/{course_id}",
//     *     name="admin_update_organization_training_course",
//     *     methods={"PUT"}
//     * )
//     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
//     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
//     * @ParamConverter("course", options={"mapping": {"course_id": "id", "training": "training"}})
//     *
//     * @param Request $request
//     * @param Organization $organization
//     * @param Training $training
//     * @param Course $course
//     * @return JsonResponse
//     */
//    public function updateOrganizationTrainingCourse(Request $request, Organization $organization, Training $training, Course $course): JsonResponse
//    {
//        $content = $request->getContent();
//        $contentObject = json_decode($content);
//
//        $course = $this->serializer->deserialize(
//            $request->getContent(),
//            Course::class,
//            'json',
//            [
//                'groups' => ['course_create'],
//                'object_to_populate' => $course
//            ]);
//
//        if (property_exists($contentObject, "path")) {
//            switch (gettype($contentObject->path)) {
//                case "NULL":
//                    $course->setPath(null);
//                    break;
//                case "integer":
//                    $pathId = $contentObject->path;
//
//                    $path = $this->entityManager
//                        ->getRepository(Path::class)
//                        ->findOneBy([
//                            "id" => $pathId,
//                            "training" => $training
//                        ]);
//
//                    if (!$path) {
//                        throw new NotFoundHttpException('No path found for id ' . $pathId);
//                    }
//
//                    $course->setPath($path);
//                    break;
//                default:
//                    throw new BadRequestHttpException('Wrong path id.');
//            }
//        }
//
//        $this->entityManager->persist($course);
//        $this->entityManager->flush();
//
//        $courseData = $this->serializer->serialize($course, 'json', ['groups' => ['course_read']]);
//
//        return new JsonResponse($courseData, 200, [], true);
//    }
}
