<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin\Organization\Training\Student;

use App\Entity\Course;
use App\Entity\Organization;
use App\Entity\Path;
use App\Entity\Session;
use App\Entity\Student;
use App\Entity\Training;
use App\Repository\StudentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use OpenApi\Annotations as OpenApi;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class OrganizationTrainingStudentController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @OpenApi\Tag(name="Organization Training Course")
     * @Route(
     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/{school_year}/students",
     *     name="admin_get_organization_training_student_list",
     *     methods={"GET"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
     * @ParamConverter("session", options={"mapping": {"training": "training", "school_year": "schoolYear"}})
     */
    public function getOrganizationTrainingStudentList(Request $request, Organization $organization, Training $training, Session $session): JsonResponse
    {
        /** @var StudentRepository $studentRepository */
        $studentRepository = $this->entityManager
            ->getRepository(Student::class);

        $studentListQueryBuilder = $studentRepository
            ->createQueryBuilder('s')
            ->innerJoin('s.groups', 'g')
            ->where('g.session = :sessionParam')
            ->setParameter(':sessionParam', $session);

        $studentListQuery = $studentListQueryBuilder
            ->getQuery();

        $page = (int) $request->query->get('page', '1');
        $limit = (int) $request->query->get('limit', '10');

        $studentListQuery
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit);

        $studentListPaginator = new Paginator($studentListQuery);

        $totalItems = $studentListPaginator->count();
        $pages = 0 === $totalItems ?
            1 :
            ceil($totalItems / $limit);

        $studentListPaginatorData = [
            'items' => $studentListPaginator,
            'totalItems' => $totalItems,
            'pages' => $pages,
        ];

        $studentListData = $this->serializer->serialize($studentListPaginatorData, 'json', ['groups' => ['user_list', 'student_list']]);

        return new JsonResponse($studentListData, 200, [], true);
    }

//
//    /**
//     * @OpenApi\Tag(name="Organization Training Course")
//     * @Route(
//     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/courses/{course_id}",
//     *     name="admin_get_organization_training_course",
//     *     methods={"GET"}
//     * )
//     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
//     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
//     * @ParamConverter("course", options={"mapping": {"course_id": "id", "training": "training"}})
//     *
//     * @param Organization $organization
//     * @param Training $training
//     * @param Course $course
//     * @return JsonResponse
//     */
//    public function getOrganizationTrainingCourse(Organization $organization, Training $training, Course $course): JsonResponse
//    {
//
//        $courseData = $this->serializer->serialize($course, 'json', ['groups' => 'course_read']);
//
//        return new JsonResponse($courseData, 200, [], true);
//    }
//
//    /**
//     * @OpenApi\Tag(name="Organization Training Course")
//     * @Route(
//     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/courses",
//     *     name="admin_create_organization_training_course",
//     *     methods={"POST"}
//     * )
//     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
//     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
//     *
//     * @param Request $request
//     * @param Organization $organization
//     * @param Training $training
//     * @return JsonResponse
//     */
//    public function createOrganizationTrainingCourse(Request $request, Organization $organization, Training $training): JsonResponse
//    {
//        $course = $this->serializer->deserialize(
//            $request->getContent(),
//            Course::class,
//            'json',
//            [
//                'groups' => ['course_create'],
//            ]);
//
//        $course->setTraining($training);
//
//        $this->entityManager->persist($course);
//        $this->entityManager->flush();
//
//        $courseData = $this->serializer->serialize($course, 'json', ['groups' => ['course_read']]);
//
//        return new JsonResponse($courseData, 200, [], true);
//    }
//
//    /**
//     * @OpenApi\Tag(name="Organization Training Course")
//     * @Route(
//     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/courses/{course_id}",
//     *     name="admin_delete_organization_training_course",
//     *     methods={"DELETE"}
//     * )
//     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
//     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
//     * @ParamConverter("course", options={"mapping": {"course_id": "id", "training": "training"}})
//     *
//     * @param Request $request
//     * @param Organization $organization
//     * @param Training $training
//     * @param Course $course
//     * @return JsonResponse
//     */
//    public function deleteOrganizationTrainingCourse(Request $request, Organization $organization, Training $training, Course $course): JsonResponse
//    {
//        $this->entityManager->remove($course);
//        $this->entityManager->flush();
//
//        return new JsonResponse("", 200, [], true);
//    }
//
//    /**
//     * @OpenApi\Tag(name="Organization Training Course")
//     * @Route(
//     *     "/admin/organizations/{organization_slug}/trainings/{training_slug}/courses/{course_id}",
//     *     name="admin_update_organization_training_course",
//     *     methods={"PUT"}
//     * )
//     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
//     * @ParamConverter("training", options={"mapping": {"training_slug": "slug", "organization": "organization"}})
//     * @ParamConverter("course", options={"mapping": {"course_id": "id", "training": "training"}})
//     *
//     * @param Request $request
//     * @param Organization $organization
//     * @param Training $training
//     * @param Course $course
//     * @return JsonResponse
//     */
//    public function updateOrganizationTrainingCourse(Request $request, Organization $organization, Training $training, Course $course): JsonResponse
//    {
//        $content = $request->getContent();
//        $contentObject = json_decode($content);
//
//        $course = $this->serializer->deserialize(
//            $request->getContent(),
//            Course::class,
//            'json',
//            [
//                'groups' => ['course_create'],
//                'object_to_populate' => $course
//            ]);
//
//        if (property_exists($contentObject, "path")) {
//            switch (gettype($contentObject->path)) {
//                case "NULL":
//                    $course->setPath(null);
//                    break;
//                case "integer":
//                    $pathId = $contentObject->path;
//
//                    $path = $this->entityManager
//                        ->getRepository(Path::class)
//                        ->findOneBy([
//                            "id" => $pathId,
//                            "training" => $training
//                        ]);
//
//                    if (!$path) {
//                        throw new NotFoundHttpException('No path found for id ' . $pathId);
//                    }
//
//                    $course->setPath($path);
//                    break;
//                default:
//                    throw new BadRequestHttpException('Wrong path id.');
//            }
//        }
//
//        $this->entityManager->persist($course);
//        $this->entityManager->flush();
//
//        $courseData = $this->serializer->serialize($course, 'json', ['groups' => ['course_read']]);
//
//        return new JsonResponse($courseData, 200, [], true);
//    }
}
