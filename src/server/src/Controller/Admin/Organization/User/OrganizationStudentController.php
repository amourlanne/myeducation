<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin\Organization\User;

use App\Entity\Organization;
use App\Entity\Student;
use App\Repository\StudentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use OpenApi\Annotations as OpenApi;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class OrganizationStudentController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @OpenApi\Tag(name="Organization User")
     * @Route(
     *     "/admin/organizations/{organization_slug}/students",
     *     name="admin_get_organization_students",
     *     methods={"GET"}
     * )
     * @ParamConverter("organization", options={"mapping": {"organization_slug": "slug"}})
     */
    public function getOrganizationStudents(Request $request, Organization $organization): JsonResponse
    {
        /** @var StudentRepository $studentRepository */
        $studentRepository = $this->entityManager
            ->getRepository(Student::class);

        $organizationStudentsQueryBuilder = $studentRepository
            ->createQueryBuilder('s')
            ->where('s.organization = :organizationParam')
            ->setParameter(':organizationParam', $organization);

        if ($searchTermQueryParam = $request->query->get('searchTerm')) {
            $organizationStudentsQueryBuilder
                ->andWhere('CONCAT(s.firstName, \' \', s.lastName) LIKE :searchTermParam')
                ->orWhere('s.email LIKE :searchTermParam')
                ->setParameter(':searchTermParam', '%'.$searchTermQueryParam.'%');
        }

        if ($exceptGroupQueryParam = $request->query->get('exceptGroup')) {
            $groupStudentIdsQuery = $studentRepository
                ->createQueryBuilder('sg')
                ->select('sg.id')
                ->innerJoin('sg.groups', 'g')
                ->where('g.slug = :exceptGroupParam');

            $organizationStudentsQueryBuilder
                ->andWhere($organizationStudentsQueryBuilder->expr()->notIn('s.id', $groupStudentIdsQuery->getDQL()))
                ->setParameter(':exceptGroupParam', $exceptGroupQueryParam);
        }

        if ($fullNameQueryParam = $request->query->get('fullName')) {
            $organizationStudentsQueryBuilder
                ->andWhere('CONCAT(s.firstName, \' \', s.lastName) LIKE :fullNameParam')
                ->setParameter(':fullNameParam', '%'.$fullNameQueryParam.'%');
        }

        if ($emailQueryParam = $request->query->get('email')) {
            $organizationStudentsQueryBuilder
                ->andWhere('s.email LIKE :emailParam ')
                ->setParameter(':emailParam', '%'.$emailQueryParam.'%');
        }

        $organizationStudentsQuery = $organizationStudentsQueryBuilder
            ->getQuery();

        $page = (int) $request->query->get('page', '1');
        $limit = (int) $request->query->get('limit', '10');

        $organizationStudentsQuery
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit);

        $organizationStudentsPaginator = new Paginator($organizationStudentsQuery);

        $totalItems = $organizationStudentsPaginator->count();
        $pages = 0 === $totalItems ?
            1 :
            ceil($totalItems / $limit);

        $organizationStudentsPaginatorData = [
            'items' => $organizationStudentsPaginator,
            'totalItems' => $totalItems,
            'pages' => $pages,
        ];

        $studentsOrganizationData = $this->serializer->serialize($organizationStudentsPaginatorData, 'json', ['groups' => 'user_read']);

        return new JsonResponse($studentsOrganizationData, 200, [], true);
    }
}
