<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Admin;

use App\Entity\Admin;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Annotations as OpenApi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class UserController extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private SerializerInterface $serializer;

    public function __construct(EntityManagerInterface $entityManager, SerializerInterface $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @OpenApi\Tag(name="User")
     * @Route(
     *     "/admin/users",
     *     name="admin_get_users",
     *     methods={"GET"}
     * )
     */
    public function getUsers(): JsonResponse
    {
        $users = $this->entityManager
            ->getRepository(Admin::class)
            ->findAll();

        $usersData = $this->serializer->serialize($users, 'json', ['groups' => 'user_read']);

        return new JsonResponse($usersData, 200, [], true);
    }
}
