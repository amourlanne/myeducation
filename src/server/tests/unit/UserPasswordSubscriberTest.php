<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\unit;

use App\EventListener\Doctrine\UserPasswordSubscriber;
use App\Tests\UnitTester;
use Codeception\Test\Unit;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserPasswordSubscriberTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public function testIListenToEvents()
    {
        /** @var UserPasswordEncoderInterface $passwordEncoder */
        $passwordEncoder = $this->createMock(UserPasswordEncoderInterface::class);

        $listener = new UserPasswordSubscriber($passwordEncoder);
        $events = $listener->getSubscribedEvents();

        $this->tester->assertContains(Events::prePersist, $events);
        $this->tester->assertContains(Events::preUpdate, $events);
    }
}
