<?php

/*
 * This file is part of the MyEducation project.
 *
 * (c) Alexis Mourlanne <alexis.mourlanne@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\functional;

use App\Entity\Admin;
use App\Tests\ApiTester;
use Codeception\Test\Unit;

class LoginTest extends Unit
{
    /**
     * @var ApiTester
     */
    protected $tester;

    public function testICanAdminLogin()
    {
        $user = $this->tester->haveUser(Admin::class, [
            'plainPassword' => 'whisper',
        ]);

        $this->tester->haveHttpHeader('Accept', 'application/ld+json');
        $this->tester->haveHttpHeader('Content-Type', 'application/json');

        $this->tester->sendPOST('/admin/login', [
            'username' => $user->getEmail(),
            'password' => 'whisper',
        ]);

        $this->tester->seeResponseCodeIs(200);
        $this->tester->seeResponseIsJson();
        $this->tester->seeResponseContainsJson(['email' => $user->getEmail()]);

        $this->tester->seeCookie(getenv('JWT_COOKIE_HP'));
        $this->tester->seeCookie(getenv('JWT_COOKIE_SIGNATURE'));
    }
}
