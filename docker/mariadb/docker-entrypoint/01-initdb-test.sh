#!/bin/bash

mysql_log() {
	local type="$1"; shift
	printf '%s [%s] [Entrypoint]: %s\n' "$(date --rfc-3339=seconds)" "$type" "$*"
}

mysql_note() {
	mysql_log Note "$@"
}

docker_process_sql() {
  passfileArgs=()
	if [ '--dont-use-mysql-root-password' = "$1" ]; then
		shift
		unset MYSQL_PWD
	else
		export MYSQL_PWD=$MYSQL_ROOT_PASSWORD
	fi
	local count=5
	while [ $count -gt 0 ] && [ ! -S "${SOCKET}" ]
	do
		count=$(( $count - 1 ))
		mysql_note "Waiting for MariaDB to start, $count more seconds"
		sleep 1
	done
	# args sent in can override this db, since they will be later in the command
	if [ -n "$MYSQL_DATABASE" ]; then
		set -- --database="$MYSQL_DATABASE" "$@"
	fi

	mysql --protocol=socket -uroot -hlocalhost --socket="${SOCKET}" "$@"
	unset MYSQL_PWD
}

docker_setup_env() {
	declare -g SOCKET
	SOCKET="/var/run/mysqld/mysqld.sock"
}

# SQL escape the string $1 to be placed in a string literal.
# escape, \ followed by '
docker_sql_escape_string_literal() {
	local escaped=${1//\\/\\\\}
	echo "${escaped//\'/\\\'}"
}

docker_setup_db() {
	# Creates a custom database and user if specified
	if [ -n "$MYSQL_TEST_DATABASE" ]; then
		mysql_note "Creating test database ${MYSQL_TEST_DATABASE}"
		docker_process_sql --database=mysql <<<"CREATE DATABASE IF NOT EXISTS \`$MYSQL_TEST_DATABASE\` ;"
	fi

	if [ -n "$MYSQL_USER" ] && [ -n "$MYSQL_USER" ]; then
		mysql_note "Creating user ${MYSQL_USER}"
		# SQL escape the user password, \ followed by '
		local userPasswordEscaped=$( docker_sql_escape_string_literal "${MYSQL_PASSWORD}" )
		docker_process_sql --database=mysql --binary-mode <<-EOSQL_USER
			SET @@SESSION.SQL_MODE=REPLACE(@@SESSION.SQL_MODE, 'NO_BACKSLASH_ESCAPES', '');
			CREATE USER IF NOT EXISTS '$MYSQL_USER'@'%' IDENTIFIED BY '$userPasswordEscaped';
		EOSQL_USER

		if [ -n "$MYSQL_TEST_DATABASE" ]; then
      mysql_note "Giving user ${MYSQL_USER} access to schema ${MYSQL_TEST_DATABASE}"
      docker_process_sql --database=mysql <<<"GRANT ALL ON \`${MYSQL_TEST_DATABASE//_/\\_}\`.* TO '$MYSQL_USER'@'%' ;"
    fi

	fi
}

_main() {
  docker_setup_env "$@"
  docker_setup_db
}

_main "$@"