RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
$(eval $(RUN_ARGS):;@:)

.DEFAULT_GOAL := help

up: ## Start the project
	docker-compose up -d --build

down: ## Stop the project
	docker-compose down

relaunch: ## Relaunch the project
	docker-compose down
	docker-compose up -d --build

MAKEFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
CWD := $(dir $(MAKEFILE_PATH))

ssl-generate: ## Generate the SSL certificate and CA
	docker run -v $(CWD)docker/traefik/certs:/root/.local/share/mkcert goodeggs/mkcert -cert-file /root/.local/share/mkcert/local-cert.pem -key-file /root/.local/share/mkcert/local-key.pem "myeducation.local" "*.myeducation.local"
	openssl x509 -in docker/traefik/certs/rootCA.pem -inform PEM -out docker/traefik/certs/rootCA.crt

xdebug-enable: ## Enable XDebug
	docker-compose exec -u root php_server docker-php-ext-enable xdebug
	docker-compose restart php_server

xdebug-disable: ## Disable XDebug
	docker-compose exec -u root php_server bash -c "rm /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini"
	docker-compose restart php_server

coverage-enable: ## Enable Pcov
	docker-compose exec -u root php_server docker-php-ext-enable pcov
	docker-compose restart php_server

coverage-disable: ## Disable Pcov
	docker-compose exec -u root php_server bash -c "rm /usr/local/etc/php/conf.d/docker-php-ext-pcov.ini"
	docker-compose restart php_server

githooks-enable: ## Enable git hooks
	git config core.hooksPath .githooks

githooks-disable: ## Disable git hooks
	git config core.hooksPath .git/hooks

migrations: ## Setup migrations
	docker-compose exec -u root php_server bash -c "php bin/console doctrine:migrations:migrate -n"

migrations-test:
	docker-compose exec -u root php_server bash -c "php bin/console doctrine:migrations:migrate -n --env=test"

fixtures: ## Setup dev fixtures
	docker-compose exec -u root php_server bash -c "php bin/console hautelook:fixtures:load --purge-with-truncate -n"

fixtures-test: ## Setup dev fixtures
	docker-compose exec -u root php_server bash -c "php bin/console hautelook:fixtures:load --purge-with-truncate -n --env=test"

lint: ## Php CS Fixer lint fix
	docker-compose exec -u root php_server bash -c "php vendor/bin/php-cs-fixer fix"

node-lint: ## Node lint fix
	docker-compose exec -u root node_admin bash -c "yarn lint --fix"
	docker-compose exec -u root node_organization bash -c "yarn lint --fix"
	docker-compose exec -u root node_client bash -c "yarn lint --fix"

phpstan: ## Php CS Fixer lint fix
	docker-compose exec -u root php_server bash -c "php vendor/bin/phpstan analyse src -l 5 --no-progress"

test-server: ## Codeception tests
	docker-compose exec -u root php_server bash -c "php vendor/bin/codecept run --coverage --coverage-xml --coverage-text"

test-server-clean: ## Clear codeception tests
	docker-compose exec -u root php_server bash -c "php vendor/bin/codecept clean"

database\:drop: ## Drop database schema
	docker-compose exec -u root php_server bash -c "php bin/console doctrine:schema:drop --full-database --force"

database\:create: ## Create database schema
	docker-compose exec -u root php_server bash -c "php bin/console doctrine:schema:create"

database\:drop\:test: ## Drop test database schema
	docker-compose exec -u root php_server bash -c "php bin/console doctrine:schema:drop --full-database --force --env=test"

database\:create\:test: ## Create test database schema
	docker-compose exec -u root php_server bash -c "php bin/console doctrine:schema:create --env=test"

.PHONY: help

help:
	@grep -E '^[a-zA-Z_\\\:-]+:.*?## .*$$' $(MAKEFILE_LIST) | sed 's/\\//g' | awk 'BEGIN {FS = ": *?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
