## Ansible
### Playbooks
#### Setup web server
```bash
ansible-playbook
  -e "ssh_key_path=/home/alexismourlanne/.ssh/id_rsa.pub"
  -i ansible/inventories/hosts.yml 
  ansible/setup-server.yml 
```
#### Setup runner server
```bash
ansible-playbook
  -e "gitlab_registration_token=<registration_token>"
  -i ansible/inventories/hosts.yml 
  ansible/setup-runner.yml 
```
#### Deploy version
```bash
ansible-playbook 
  --vault-password-file ansible/.vault 
  -e "APP_VERSION=<version:master,1.0.1> DEPLOY_REGISTRY_TOKEN=<deploy_registry_token>" 
  -i ansible/inventories/hosts.yml 
  ansible/deploy.yml
```

### Vault env
#### Encrypt file
```bash
ansible-vault encrypt ansible/files/.env
> $ANSIBLE_VAULT_PASSWORD
```

#### Edit file
```bash
ansible-vault edit ansible/files/.env
> $ANSIBLE_VAULT_PASSWORD
```

### Gitlab Continuous Integration

| Environment variable |   |
|------------------------|---|
| DEPLOY_SSH_PRIVATE_KEY |  SSH private key to *www-data* server user. |
| DEPLOY_REGISTRY_TOKEN  |  Settings → Repository → Deploy tokens → Create deploy token (with *read_repository* access) |
| ANSIBLE_VAULT_PASSWORD |  Ansible vault password generate to encrypt file. |